# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 15:49:32 2018

@author: marc
"""
import scipy as sp
import glob
import math
import os
from ymraster import Raster
import ogr
import gdal
import osgeo.osr as osr
import multiprocessing as mp
import copy_reg
import types

def rescale_data(data, dstmin=0, dstmax=1, percentile_srcmin=2,
                 percentile_srcmax=98, nodata=None, axis=None):
    """
    Rescale an array

    Parameters
    ----------
    data : ndarray,
        Array to rescale.
    dstmin : float
        Minimum rescaled value. Default to 0
    dstmax : float
        Maximum rescaled value. Default to 1
    percentile_srcmin : int
        Value of percentile_srcmin Percentile of the orginal data will be taken
        as minimum. Default to 2
    percentile_srcmax : int
        Value of percentile_srcmax Percentile of the orginal data will be taken
        as maximum. Default to 2
    nodata : float,
        If indicated, values equals to nodata wont be considered in the
        rescaling
    axis : int or None,
        Axis along which the function is applied:

            * 0 : each column is rescaled seperatly
            * 1 : each row is rescaled seperatly.
            * None : the whole array is rescaled
        Default to None
    Returns
    --------
    rescaled_array : ndarray,
        Rescaled array of the same size a data.

    """
    if axis == 0:
        for col in data.T:
            temp = col.copy()
            col[:] = rescale_data_1d(temp, dstmin=dstmin, dstmax=dstmax,
                                  percentile_srcmin=percentile_srcmin,
                                  percentile_srcmax=percentile_srcmax,
                                  nodata=nodata)
    elif axis == 1:
        for row in data.T:
            row[:] = rescale_data_1d(row, dstmin=dstmin, dstmax=dstmax,
                                  percentile_srcmin=percentile_srcmin,
                                  percentile_srcmax=percentile_srcmax,
                                  nodata=nodata)
    elif not axis:
        data = rescale_data_1d(data, dstmin=dstmin, dstmax=dstmax,
                            percentile_srcmin=percentile_srcmin,
                            percentile_srcmax=percentile_srcmax,
                            nodata=nodata)

    return data

def rescale_data_1d(data, dstmin=0, dstmax=1, percentile_srcmin=2,
                    percentile_srcmax=98, nodata=None):
    """
    """
    # initialize array
    data_rescaled = sp.empty_like(data)

    # get the data min and max values to take into account
    if nodata:
        data = sp.ma.masked_where(data == nodata, data)
        data = sp.ma.filled(data, sp.nan)
    srcmin = sp.nanpercentile(data, percentile_srcmin)
    srcmax = sp.nanpercentile(data, percentile_srcmax)

    # rescale
    data_rescaled = (dstmin + ((dstmax - dstmin) /
                     (srcmax - srcmin)) * (data - srcmin))

    # cut to min and max values of the rescaled data
    data_rescaled = sp.where(data_rescaled <= 1, data_rescaled, 1)
    data_rescaled = sp.where(data_rescaled >= 0, data_rescaled, 0)

    return data_rescaled


def create_grid(output_grid, ref_raster, cell_size, epsg=2154):
    """
    Create a shapefile grid with the same extent than a reference raster file

    Paremeters
    ----------
    output_grid : string
        Path of output shapefile grid
    ref_raster : string
        Path of input reference raster file.
    cell_size : float
        Size of elementary grid cells in projection unit.
    epsg : int,
        EPSG code. Default to Lambert 93 projection (2154).
    """

    rst = Raster(ref_raster)
    xmin, xmax, ymin, ymax = rst.extent

    # get rows
    rows = (ymax - ymin) // cell_size
    # get columns
    cols = (xmax - xmin) // cell_size

    # start grid cell envelope
    ringXleftOrigin = xmin
    ringXrightOrigin = xmin + cell_size
    ringYtopOrigin = ymax
    ringYbottomOrigin = ymax - cell_size

    # create output file
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg)
    outDriver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(output_grid):
        os.remove(output_grid)
    outDataSource = outDriver.CreateDataSource(output_grid)
    outLayer = outDataSource.CreateLayer(output_grid, srs,
                                         geom_type=ogr.wkbPolygon)
    featureDefn = outLayer.GetLayerDefn()

    # create a field
    fieldName = 'id'
    fieldType = ogr.OFTInteger
    idField = ogr.FieldDefn(fieldName, fieldType)
    outLayer.CreateField(idField)

    # create grid cells
    countcols = 0
    while countcols < cols:
        countcols += 1

        # reset envelope for rows
        ringYtop = ringYtopOrigin
        ringYbottom = ringYbottomOrigin
        countrows = 0

        while countrows < rows:
            current_id = cols * countrows + (countcols - 1)
            countrows += 1

            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(ringXleftOrigin, ringYtop)
            ring.AddPoint(ringXrightOrigin, ringYtop)
            ring.AddPoint(ringXrightOrigin, ringYbottom)
            ring.AddPoint(ringXleftOrigin, ringYbottom)
            ring.AddPoint(ringXleftOrigin, ringYtop)
            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)

            # add new geom to layer
            outFeature = ogr.Feature(featureDefn)
            outFeature.SetGeometry(poly)
            outFeature.SetField('id', current_id)
            outLayer.CreateFeature(outFeature)
            outFeature = None

            # new envelope for next poly
            ringYtop = ringYtop - cell_size
            ringYbottom = ringYbottom - cell_size

        # new envelope for next poly
        ringXleftOrigin = ringXleftOrigin + cell_size
        ringXrightOrigin = ringXrightOrigin + cell_size

    # Save and close DataSources
    outDataSource = None

def generate_id_from_coord(coord):
    """
    Generate an id from a set of coordinate.

    Parameters
    ----------
    coord : list of tuple of int,
        For instance list of (id_row, id_col).

    Returns
    -------
    my_id : str
        Id of set of tile

    Limitations
    -----------
    my_id is not unique by constuction and two set of different coord might
    have a same id yet is very unlikely.
    """
    a_sum = 0

    for id_row, id_col in coord:
        a_sum += id_row + id_col

    my_id = (str(len(coord)) + str(coord[0][0]) + str(coord[-1][1]) +
             str(a_sum))

    return my_id



def get_pixel_coord_from_points(vector, raster, field_name):
    """
    For points in a shapefile, return the pixel localisation (row, col) on the
    given raster for each point located in the raster.

    Parameters
    ----------
    vector : str
        Path of the input point vector. All the points must be in the raster.
    field_name : str,
         Name of the field contaning the id of the point.
    raster : str
        Path of the input raster

    Return
    ------
    pix_cood : dict
        Dictionary of pixel coordinates on the raster. {id_point : (row,col)}.
        If the point is not in the raster, nothing is indicated.
    coord : dict
        Dictionary of geographic coordinates in the EPSG. {id_point : (x,y)}
    """

    # Get geographic coordinates of each point
    coord = get_geographic_coord_from_points(vector, field_name = field_name)

    # Get the pixel coordinate of each point on the raster
    raster_ = gdal.Open(raster)
    geotransform = raster_.GetGeoTransform()
    originX = geotransform[0]    # Upper left corner xcoordinate
    originY = geotransform[3]    # Upper left corner ycoordinate
    pixelWidth = geotransform[1]
    pixelHeight = - geotransform[5]

    # Get pix coord of each point. They are inverted regarding geographic
    # coordinates ((y, x) instead of (x, y)), in order to read the array.
    pix_coord = {}    #Dict of coordinates {id_of_point : (row, col)}
    for id_pt, coord_pt in coord.items():
        x, y = coord_pt
        col = int((x - originX) / pixelWidth)
        row = int((originY - y) / pixelHeight)
        if col >= 0 and row >= 0:
            pix_coord[id_pt] = (row, col)    # Coord are inverted to read the array

    return pix_coord, coord


def get_geographic_coord_from_points(vector, field_name = None):
    """
    Get the geographic coordinates of a point shapefile.
    Parameters
    ----------
    vector : str
        Path of the input shapefile.
    field_name : str, optional
        Field of the shapefile containing the id of each point.

    Return
    ------
    coord : list or dict
        If field_name is not indicated, list of coordinates (tuple) : [(x,y)].
        Else, dict of coordinates : {id_point : (x,y)}
    """
    # Get geographic coordinates of each point
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(vector, 0)
    layer = dataSource.GetLayer()

    if not field_name:
        coord = []
    else:
        coord = {}
    for feature in layer:
        geom = feature.GetGeometryRef()
        pt = geom.Centroid().ExportToWkt()    # String
        coord_f = eval(','.join(pt.split(' ')[1:]))    # Trasform into tuple
        if not field_name:
            coord.append(coord_f)
        else:
            coord[feature.GetField(field_name)] = coord_f
    driver = None
    return coord

def pol_coord_from_xy(x, y):
    """
    Get polar coordinates from x y Cartesian coordinates

    Parameters
    ----------
    x : array
        x coordinates
    y : array
        y coordinates

    Returns
    -------
    pol : dictionnary of array.
        Polar coordinates : ::

            {'theta': array of theta (in radians),
             'r' : array of distance from center
            }
    """
    pi = sp.pi

    theta = sp.empty(x.shape)
    theta = sp.where((x < 0) * (y > 0), pi - sp.arctan(- y / x), theta)
    theta = sp.where((x < 0) * (y < 0), pi + sp.arctan(y / x), theta)
    theta = sp.where((x > 0) * (y < 0), 2 * pi - sp.arctan(- y / x), theta)
    theta = sp.where((x > 0) * (y > 0), sp.arctan(y/x), theta)
    r = sp.sqrt(x**2 + y**2)
    pol = {'r': r,
           'theta': theta}

    return pol

def add_suffix(path_file, suffix):
    """
    Parameters
    ----------
    path_file : str
        A file path
    suffix : str
        Suffix to add at the end of a filename

    Returns
    -------
    path_file_suffix : str
        Path file with suffix add

    Example
    -------
        >>> suffix = 'suffix'
        >>> path_file = 'tata/toto.tif'
        >>> suffix = 'suffix'
        >>> path_file_suffix = add_suffix(path_file, suffix)
        >>> print path_file
        tata/toto.tif
        >>> print path_file_suffix
        tata/totosuffix.tif
    """
    begin, ext = os.path.splitext(path_file)
    path_file_suffix = begin + suffix + ext

    return path_file_suffix


def ang_offset(value, offset):
    """
    Compute an offset of on angular values and convert in degrees
    """
    return (math.degrees(value) + offset) % 360

def compute_logration_rspectra(r_spectra1, r_spectra2):
    """
    """
    ratio = r_spectra1 / r_spectra2
    log_ratio = sp.apply_along_axis(math.log, 1, ratio.reshape(-1,1))

    return log_ratio

class Param_loader(object):
    """
    """
    _PATTERN = {}

    _PATTERN['coorditot'] = 'coorditot.csv'
    _PATTERN['cosv'] = 'cosv.csv'
    _PATTERN['eigtab'] = 'eigtab.csv'
    _PATTERN['vp'] = 'vp.csv'
    _PATTERN['eigvec'] = 'eigvec.csv'
    _PATTERN['ctri'] = 'ctri.csv'
    _PATTERN['ctrv'] = 'ctrv.csv'
    _PATTERN['cos2i'] = 'cos2i.csv'
    _PATTERN['normvec'] = 'normvec.csv'
    _PATTERN['spectreswima'] = 'spectreswima.csv'
    _PATTERN['centeredsp'] = 'centeredsp.csv'

    def __init__(self, fold_data, list_param, n_process=1):
        """
        """
        self._fold_data = fold_data
        if n_process > 1:
            self.init_multi_process(fold_data, list_param, n_process)
        else:
            self.init_one_process(fold_data, list_param)

    def init_multi_process(self, fold_data, list_param, n_process):
        """
        """
        pool = mp.Pool(processes=n_process)
        result = [pool.apply_async(self.load_file,
                                   args=(self._PATTERN[param],)
                                   )
                  for param in list_param]

        pool.close()
        pool.join()
        for param, r in zip(list_param, result):
            tab = r.get()
            setattr(self, param, tab)

    def init_one_process(self, fold_data, list_param):
        """
        """
        for param in list_param:
            setattr(self, param, self.load_file(self._PATTERN[param]))

    def load_file(self, pattern):
        """
        """
        is_fold_printed = False
        try:
            csv_path = glob.glob(os.path.join(self._fold_data,
                                              '*' + pattern))[0]
            return sp.loadtxt(csv_path, dtype='float', delimiter=',')

        except:
            if not is_fold_printed:
                print self._fold_data
                is_fold_printed = True
            msg = 'Warning : {} - like file has not been loaded'
            print msg.format(pattern)
            return None


# this part is for allowing multiprocessing on instance method
def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)


def _unpickle_method(func_name, obj, cls):
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)

# add to the registry
copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)
