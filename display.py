# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 14:46:20 2018

@author: marc
"""

import scipy as sp
import os
from pyfoto import lib
from itertools import product
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib.cm as cmx
from os.path import join
import math

def ang_offset(value, offset):
    """
    Compute an offset of  on angular values
    """
    return (value + offset) % 360

def plot_pca(r_data, **kwargs):
    """Plot a rescaled dataset in the PCA space.

    Parameters:
    -----------
    r_data : 2d array, [n_ind, n_dim]
        2D array containing the rescaled data (n_ind are in rows,
        the n_dim axes of PCA in columns)
    kwargs :
        eigval : 1d array, [n_dim] (Optional)
            1d array containing eigenvalues of the PCA. If not indicate,
            the part of the variance of each axes will not be plot.
        axes : list of int, optional
            List of the number of axes that sould be plot. Indexation starts
            at 1. Default to [1,2]. If -1 is indicated, it's mean lumiance that
            is taken into account.
        title : string, optional
            Common name of the plot. The complete name will be title +
            'Axe {}{}' where {} is the axe number. Default to None
        color : nd_array [n_ind, 3] or list of RGB values,
            RBG values for each point. Values are comprised between 0 and 1, or
            list of rgb values if tab_markers is indicated (one for each
            marker)
        tab_markers : 1d array,
            Tab of same lenght of first fim of r_data that contain marker of
            each point.
        plot_marker : bool,
            If True marker in tab_markers will be plot. Defaults to False.
        out_filename: string, (optional)
            If indicated, the plot will be saved
        ax : matplotlib.axes._subplots.AxesSubplot object (optional)
            If indicated, plot will be drawn in axe provided. Else en new
            matplotlib figure is created.
    Scatter plot parameters:
        ========= =============================================================
        kwargs    Description
        ========= =============================================================
        alpha     int [0,1], transparency of dot. Default to 1.
        s         int, size of dot.
        cmap      color range to use. default to 'jet'. Used only if tab_color
                  is dimension 1.
        ========= =============================================================
    """

    # initialize parameters
    axes = kwargs.get('axes', [1, 2])
    eigval = kwargs.get('eigval', None)
    out_filename = kwargs.get('out_filename', False)
    tab_color = kwargs.get('color', None)
    axe1 = axes[0] - 1 if axes[0] != -1 else -1
    axe2 = axes[1] - 1 if axes[1] != -1 else -1

    # define plot arguments
    plot_args = {}
    plot_args['s'] = kwargs.get('s') or (85 if kwargs.get('tab_markers')
                                         is not None else 10)
    plot_args['alpha'] = kwargs.get('alpha') or 1
    if tab_color is not None:
        try:
            _ = iter(tab_color[0])
        except TypeError:
            plot_args['cmap'] = kwargs.get('cmap') or 'jet'

    # plot data
    if kwargs.get('ax'):
        ax = kwargs.get('ax')
        subplot = True
    else:
        ax = plt.subplots()[1]
        subplot = False

    if kwargs.get('tab_markers') is not None:
        list_markers = sp.unique(kwargs.get('tab_markers'))
        tab_markers = kwargs.get('tab_markers')
        for mark, col in zip(list_markers, tab_color):
            # extract the coordinates element corresponding to mark
            coord = sp.where(tab_markers == mark)[0]
            # plot it
            if kwargs.get('plot_marker') is not None:
                ax.scatter(r_data[:, axe1][coord], r_data[:, axe2][coord],
                           marker=r"$ {} $".format(int(mark)), c=col,
                           **plot_args)
            else:
                ax.scatter(r_data[:, axe1][coord], r_data[:, axe2][coord],
                           c=col, **plot_args)
    else:
        ax.scatter(r_data[:, axe1], r_data[:, axe2], c=tab_color,
                   edgecolors='face', **plot_args)

    # plot labels, title etc
    if plot_args.get('cmap'):
        ax.colorbar()

    if axe2 != - 1:
        if eigval is not None:
            var_explained = round(eigval[axe2] / sum(eigval) * 100, 2)
            var_explained = '({} %)'.format(var_explained)
        else:
            var_explained = ''
        y_label = 'PC{} {}'.format(axe2 + 1, var_explained)
        ax.axhline(0, color="gray", ls='dashed')
    else:
        y_label = 'MEAN grey level'
    if axe1 != - 1:
        if eigval is not None:
            var_explained = round(eigval[axe1] / sum(eigval) * 100, 2)
            var_explained = '({} %)'.format(var_explained)
        else:
            var_explained = ''
        x_label = 'PC{} {}'.format(axe1 + 1, var_explained)
        ax.axvline(0, color="gray", ls='dashed')
    else:
        x_label = 'MEAN grey level'

    ax.set_ylabel(y_label)
    ax.set_xlabel(x_label)
    ax.set_facecolor('ivory')

    if kwargs.get('title') and not subplot:
        ax.set_title(kwargs['title'] + ',\n axes {} & {}'.format(axe1 + 1,
                                                                 axe2 + 1))
    if out_filename:
        plt.savefig(out_filename)

    if subplot:
        return ax
    else:
        plt.show()

def plot_polar_plan(r_data, **kwargs):
    """Plot a rescaled dataset in the PCA space.

    Parameters:
    -----------
    r_data : 2d array, [n_ind, n_dim]
        2D array containing the rescaled data (n_ind are in rows,
        the n_dim axes of PCA in columns)
    kwargs :
        eigval : 1d array, [n_dim] (Optional)
            1d array containing eigenvalues of the PCA. If not indicate,
            the part of the variance of each axes will not be plot.
        axes : list of int, optional
            List of the number of axes that sould be plot. Indexation starts
            at 1. Default to [1,2]. If -1 is indicated, it's mean lumiance that
            is taken into account.
        title : string, optional
            Common name of the plot. The complete name will be title +
            'Axe {}{}' where {} is the axe number. Default to None
        color : nd_array [n_ind, 3] or list of RGB values,
            RBG values for each point. Values are comprised between 0 and 1, or
            list of rgb values if tab_markers is indicated (one for each
            marker)
        tab_markers : 1d array,
            Tab of same lenght of first fim of r_data that contain marker of
            each point.
        out_filename: string, (optional)
            If indicated, the plot will be saved
        ax : matplotlib.axes._subplots.AxesSubplot object (optional)
            If indicated, plot will be drawn in axe provided. Else en new
            matplotlib figure is created.
    Scatter plot parameters:
        ========= =============================================================
        kwargs    Description
        ========= =============================================================
        alpha     int [0,1], transparency of dot. Default to 1.
        s         int, size of dot.
        cmap      color range to use. default to 'jet'. Used only if tab_color
                  is dimension 1.
        ========= =============================================================
    """

    # initialize parameters
    axes = kwargs.get('axes', [1, 2])
    out_filename = kwargs.get('out_filename', False)
    tab_color = kwargs.get('color', None)
    axe1 = axes[0] - 1 if axes[0] != -1 else -1
    axe2 = axes[1] - 1 if axes[1] != -1 else -1

    # define plot arguments
    plot_args = {}
    plot_args['s'] = kwargs.get('s') or (85 if kwargs.get('tab_markers')
                                         is not None else 10)
    plot_args['alpha'] = kwargs.get('alpha') or 1
    if tab_color is not None:
        try:
            _ = iter(tab_color[0])
        except TypeError:
            plot_args['cmap'] = kwargs.get('cmap') or 'jet'

    # plot data
    if kwargs.get('ax'):
        ax = kwargs.get('ax')
        subplot = True
    else:
        ax = plt.subplots()[1]
        subplot = False

    if kwargs.get('tab_markers') is not None:
        list_markers = sp.unique(kwargs.get('tab_markers'))
        tab_markers = kwargs.get('tab_markers')
        for mark, col in zip(list_markers, tab_color):
            # extract the coordinates element corresponding to mark
            coord = sp.where(tab_markers == mark)[0]
            # plot it
            #ax.scatter(r_data[:, axe1][coord], r_data[:, axe2][coord],
            #           marker=r"$ {} $".format(int(mark)), c=col, **plot_args)
            ax.scatter(r_data[:, axe1][coord], r_data[:, axe2][coord],
                       c=col, edgecolors='none', **plot_args)
    else:
        ax.scatter(r_data[:, axe1], r_data[:, axe2], c=tab_color,
                   edgecolors='none', zorder=2, **plot_args)

    # plot labels, title etc
    if plot_args.get('cmap'):
        ax.colorbar()

    if axe1 == 0 and kwargs.get('plot_ang_limit'):
        for ang_lim in range(45, 360, int(45)):
            ax.axvline(ang_lim, color='darkgoldenrod', linestyle='--',
                       linewidth=1, zorder=1)
            if kwargs.get('plot_ang_limit_label'):
                label = range(1, 9)
                ax.set_xticks(range(int(45 / 2) ,361, int(45)))
                ax.set_xticklabels(label, color='darkslategrey')
                ax.tick_params(bottom=False)

    ax.set_facecolor('ivory')
#    ax.spines["top"].set_color('darkslategrey')
#    ax.spines["right"].set_color('darkslategrey')
#    ax.spines["bottom"].set_color('darkslategrey')
#    ax.spines["left"].set_color('darkslategrey')
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)
    #ax.tick_params(axis='x', colors='w')
    ax.tick_params(axis='y', colors='darkslategrey')

    if kwargs.get('title') and not subplot:
        ax.set_title(kwargs['title'] + ',\n axes {} & {}'.format(axe1 + 1,
                                                                 axe2 + 1))
    if out_filename:
        plt.savefig(out_filename)

    if subplot:
        return ax
    else:
        plt.show()


def get_norme_difference(foto1, foto2, axes=[1, 2]):
    """
    """
    # get coord of foto2 in same PCA space of foto1
    coord_foto2 = foto2.pca.apply_other_pca_to_rspectrum(foto1.pca)
    coord_foto1 = foto1.pca.coorditot

    # get their position differences
    dif = coord_foto1 - coord_foto2

    # get their norme difference
    ax1 = axes[0] - 1
    ax2 = axes[1] - 1
    norme = sp.hypot(dif[ax1], dif[ax2])
    return norme


def plot_norm_dif(foto_1, foto_2, axes=[1, 2], out_filename=None,
                  **plot_kwargs):
    """
    """
    title = 'Norme difference'
    color = get_norme_difference(foto_1, foto_2, axes=axes)
    color = lib.rescale_data(color)
    data = foto_1.pca.coorditot
    plot_pca(data, axes=axes, title=title, out_filename=out_filename,
             color=color, **plot_kwargs)


def compare_loading(foto_1, foto_2, norm_x=True, axes=[1, 2, 3],
                    mode='curve', out_filename=None, foto_label='auto',
                    ax=None, axe_label='auto'):
    """
    Compare PCA loading of two Foto instance

    Parameters
    ----------
    foto_1 : `Foto` instance
        Foto instance to compare with 'foto_2'
    foto_2 : `Foto` instance
        Foto instance to compare with 'foto_1'
    norm_x : bool, (optional)
        If True, the x axis scale will be set to log-scale. Default to True
    axes : list, (optional)
        List of the princpal component index to plot. Indexation stats at
        1. Default to the 3 first components.
    out_filename : str (optional)
        Path of output plot. If not indicated, figure is not saved.
    mode : str ``<'fill', 'curve'>`` (optional)
        If loading are fill or not under the curve.
    foto_label : tuple of str
        Label of for the two foto instance. Defauts to ('foto 1', 'foto 2') if
        "auto" indicated.
    ax : `matplotlib.axes._subplots.AxesSubplot` instance (optional)
        If indicated, plot will be drawn in axe provided. Else, a new
        matplotlib figure is created.
    axe_label : tuple (x_label, y_label) (optional)
        x and y Label. Defautl label are used if set to "auto".
    """

    LIST_MODE = ('fill', 'curve')
    STYLE = {LIST_MODE[0]: ('solid', 'dashed'),
             LIST_MODE[1]: ('solid', 'dashed')}
    color = ['crimson', 'lime', 'dodgerblue']
    dif_color = ['brown', 'green']

    # check input parameters
    if mode not in LIST_MODE:
            msg = "'mode' parameters has to be one of these choices {}"
            raise ValueError(msg.format(LIST_MODE))

    # deals with legend labels
    labels = ['1st axis', '2nd axis', '3rd axis']
    if foto_label:
        foto_label = foto_label if foto_label != 'auto' else ('foto 1',
                                                              'foto 2')
        labels_1 = ['{}: {}'.format(foto_label[0], l) for l in labels]
        labels_2 = ['{}: {}'.format(foto_label[1], l) for l in labels]
    else:
        labels_1 = [None, None, None]
        labels_2 = [None, None, None]

    # If subplot axis instance was passed in the call or not
    if ax:
        subplot = True
        my_ax = ax
    else:
        my_ax = plt.subplots()[1]
        subplot = False

    # log scale or not
    if norm_x:
        my_ax.set_xscale('log')

    # load data to plot
    f_km_1 = foto_1.cycles_km
    f_km_2 = foto_2.cycles_km
    load_1 = foto_1.pca.loading
    load_2 = foto_2.pca.loading

    # iterate over PC axes to plot
    for col, ax, dif_col in zip(color, axes, dif_color):
        num_ax = ax - 1

        # plot curve
        my_ax.plot(f_km_1, load_1[:, num_ax].transpose(),
                   label=labels_1[num_ax], color=col, ls=STYLE['curve'][0],
                   zorder=5)
        my_ax.plot(f_km_2, load_2[:, num_ax].transpose(),
                   label=labels_2[num_ax], color=col, ls=STYLE['curve'][1],
                   zorder=5)

        # fill beween curve and 0 x line
        if mode == 'fill':
            y1 = load_1[:, num_ax].transpose()
            y2 = load_2[:, num_ax].transpose()
            my_ax.fill_between(f_km_1, 0, y1,
                               label=None, alpha=0.25,
                               facecolor=col, edgecolor='gainsboro',
                               interpolate=True, zorder=1)
            my_ax.fill_between(f_km_1, 0, y2,
                               label=None, alpha=0.25,
                               facecolor=col, edgecolor='gainsboro',
                               interpolate=True, zorder=2)

    # fill between curve
    if mode == 'fill':
        for col, ax, dif_col in zip(color, axes, dif_color):
            num_ax = ax - 1
            y1 = load_1[:, num_ax].transpose()
            y2 = load_2[:, num_ax].transpose()
            # clean in case it was already filled (because the actual color
            # as transparency)
            my_ax.fill_between(f_km_1, y1, y2,
                               label=None,
                               facecolor='white',
                               interpolate=True,
                               zorder=3)

            # fill with the appropriate color
            my_ax.fill_between(f_km_1, y1, y2,
                               label=None, alpha=0.65,
                               facecolor=dif_col,
                               interpolate=True,
                               zorder=4)


    # plot legend, title and axes label
    my_ax.legend(loc=0,  fontsize='small')
    my_ax.axhline(0, color="gray", ls='dashed')
    my_ax.set_title('')
    AXE_LABEL_DEFAULT = ('Spatial frequencies (km-1)', 'Scaled PCA loading')
    _plot_axe_label(axe_label, AXE_LABEL_DEFAULT, my_ax)

    if out_filename:
        plt.savefig(out_filename)

    # retur subplot axes instance or plot it
    if subplot:
        return my_ax
    else:
        plt.show()

def compare_var_explained(foto_1, foto_2, ax=None, n_axis='all',
                          axe_label='auto', legend='Cumulative sum',
                          show_var_label=True):
    """
    Compare bar of variance and cumulative variances explained by first axes
    of two Foto instance

    Paremeters
    ----------
    foto_1 : `Foto` instance
        Foto instance to compare with 'foto_2'
    foto_2 : `Foto` instance
        Foto instance to compare with 'foto_1'
    n_axis : int or str,
        If "all", all axes are drawn, if "int" only the n_axis first.
    legend : str (optional)
        Legend.
    ax : `matplotlib.axes._subplots.AxesSubplot` instance (optional)
        If indicated, plot will be drawn in axe provided. Else en new
        matplotlib figure is created.
    axe_label : tuple (x_label, y_label) (optional)
        x and y Label. Defautl label are used if set to "auto".
    show_var_label : Bool (optional)
        If True, text values of explained variance for the three first axes
        are drawn.
    """

    # load data
    foto_1.pca.compute_var_explained()
    foto_2.pca.compute_var_explained()
    var = []
    var.append(sp.diag(foto_1.pca.var_explained) * 100)
    var.append(sp.diag(foto_2.pca.var_explained) * 100)

    # filter data if necessary
    if n_axis != 'all':
        try:
            var = [v[:n_axis] for v in var]
        except IndexError:
            msg = ('Integer value is expected for `n_axis` ' +
                   'not : {}'.format(n_axis))
            raise TypeError(msg)
    else:
        n_axis = var[0].shape[0]
    var_cum_sum = [sp.cumsum(v) for v in var]

    # If subplot axis instance was passed in the call or not
    if ax:
        ax = ax
        subplot = True
    else:
        ax = plt.subplots()[1]
        subplot = False

    x = sp.arange(n_axis) + 1
    ax.bar(x - 0.2, var[0], 0.4, color='royalblue', align='center',)
    ax.bar(x + 0.2, var[1], 0.4, color='firebrick', align='center',)
    ax.plot(x, var_cum_sum[0], color='royalblue', ls='dashed',
            label=legend)
    ax.plot(x, var_cum_sum[1], color='firebrick', ls='dashed',
            label=legend)

    # plot variance amout of the tree first component in text
    if show_var_label:
        for v1, v2, x_pos in zip(var[0][:3], var[1][:3], x):
            ax.text(x_pos - 0.3, v1, '{:.1f}'.format(v1))
            ax.text(x_pos + 0.3, v2, '{:.1f}'.format(v2))

    AXE_LABEL_DEFAULT = ('Axes', 'Variance explained (in %)')
    _plot_axe_label(axe_label, AXE_LABEL_DEFAULT, ax)

    if legend:
        ax.legend(loc=0)
    if subplot:
        return ax
    else:
        plt.show()


def _plot_axe_label(axe_label, axe_label_default, ax):
    """
    Manage axes labels

    Paremeters
    ----------
    axe_label : tuple (x_label, y_label) or str
        Axes label. If "auto" : `axe_label_default` are used
    axe_label_default,
        tuple (x_label, y_label)
    ax : Matplotlib.axes._subplots.AxesSubplot object
        Plot will be drawn in axis provided.
    """
    if axe_label:
        x_label = axe_label[0] if axe_label != 'auto' \
                               else axe_label_default[0]
        y_label = axe_label[1] if axe_label != 'auto' \
                               else axe_label_default[1]
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)

class Foto_displayer():
    """
    TODO : reflechir à l'outfilename
    """
    def __init__(self, foto):
        self.foto = foto

    def correlation_circle(self, axes=[1, 2], unit='n_occ', save=False,
                           add_point_coord=None, add_point_color=None,
                           add_point_label=None, fontsize=None,
                           legend_color=None, figsize=None,
                           out_filename=None, ax=None, circle=True):
        """
        Plot the correlation circle of the PCA

        Parameters
        ----------
        axes : list of axes, (optional)
            Contains the numbers of the axes to plot. Default to [1,2]
        unit : str {'n_occ','km'}, (optional)
            In which unit to display the label of the variables. If None, no
            label will be displayed. Default to 'n_occ'.
        save : bool, (optional)
            If true the plot will be saved, with predetermined name
        out_filename : str (optional)
            Path of output plot.
        ax : `matplotlib.axes._subplots.AxesSubplot` instance (optional)
            If indicated, plot will be drawn in axe provided. Else en new
            matplotlib figure is created.
        """

        axes = [a - 1 for a in axes]

        # Define labels unit
        if unit == 'n_occ':
            lab = range(1, self.foto.fourier.nb_f + 1)
        elif unit == 'km':
            lab = self.foto.cycles_km
        else:
            lab = [None for f in range(1, self.foto.fourier.nb_f + 1)]

        # fig, ax = plt.subplots(nrows=1, ncols=1, figsize=figsize)

        if ax:
            ax = ax
            subplot = True
        else:
            ax = plt.subplots()[1]
            subplot = False

        # plot each axes
        for elem in range(self.foto.fourier.nb_f):
            x = self.foto.pca.cosv[elem, axes[0]]
            y = self.foto.pca.cosv[elem, axes[1]]
            ax.plot(x, y, '.', color='black')
            if unit:
                ax.text(x, y, '{}'.format(lab[elem]), fontsize=fontsize,
                        color='darkslategrey')

        # TODO : to export this part out of the function
        if add_point_coord:
            for i, (x, y) in enumerate(add_point_coord):
                color = add_point_color[i] if add_point_color else None

                ax.plot(x, y, '.', color=color)
                if add_point_label:
                    ax.text(x, y, add_point_label[i], fontsize=fontsize,
                             color=color)
            if legend_color:
                for i, (cla, col) in enumerate(legend_color.iteritems()):
                    ax.text(0.9, 0.9 - i / 20., str(cla), color=col,
                             weight='bold')

        # plot the circle and the legend
        ax.set_xlim(-1, 1)
        ax.set_ylim(-1, 1)
        if circle:
            circle = plt.Circle((0, 0), 1, fill=False) #, ls='dotted')
            #fig.gca().add_artist(circle)
            ax.add_patch(circle)
            ax.axhline(0, color="gray")#, ls='dashed')
            ax.axvline(0, color="gray")#, ls='dashed')
        if not subplot:
            ax.set_xlabel('Principal component n {}'.format(axes[0] + 1))
            ax.set_ylabel('Principal component n {}'.format(axes[1] + 1))
            ax.set_title('PC correlation / Initial axes')
        ax.set_facecolor('ivory')

        if out_filename:
            plt.savefig(out_filename)
        elif save:
            txt = ''
            for a in axes:
                txt = txt + str(a + 1)
            plt.savefig(join(self.foto.output_fold,
                             'circle_correl_axes{}.png'.format(txt)))

        if subplot:
            return ax
        else:
            plt.show()

    def get_color_table(self, axes=[1, 2, 3], score=None):
        """
        Create a color table for the axes considered. Each axis colors are
        streched to +- 2% of his min and max values.

        Parameters
        ----------
        num_axis : list of int,
            Number of the nth PCA axis to use. Indexation starts at 1. Defaults
            to [1,2,3], if -1 is indicated, it's the mean of spectral values
            that will be used.
        score = string {`raw`,`std`} (optional)
            If raw scores or standardized scores are plot. Default to
            Foto_dispayer.foto.score_mode.

        Returns
        -------
        color_table : sp.array (n, 3),
            Contains the corresponding RGB composite values for each windows.
            Colors are comprized between 0 and 1.
        """
        color_table = sp.empty((self.foto.pca.coorditot.shape[0], 3))
        if len(axes) == 2:
            color_table[:, 2] = 0.5
        score = score or self.foto.score_mode
        if score == 'raw':
            tab_score = self.foto.pca.coorditot
        elif score == 'std':
            tab_score = self.foto.pca.std_coorditot

        for i, axis in enumerate(axes):
            if axis == -1:
                col = self.foto.mean_lum_by_window
                # change sign of mean lum if vegetation appears in dark on
                # the image used
                if self.foto.in_img.veg_lum.veg_color == 'dark':
                    msg = ('Sign of mean grey level is changed since ' +
                           'vegeation appears as dark in original input image')
                    print msg
                    col = [- mean_lum for mean_lum in col]
            else:
                axis -= 1
                col = tab_score[:, axis]

            color_table[:, i] = lib.rescale_data(col, dstmin=0, dstmax=1)

        return color_table

    def pca_plan(self, axes=[1, 2], title='PCA plan of', label=False,
                 rgb_compo=None, save=False, score=None,
                 angular_colors=None, plot_marker=None, out_filename=None,
                 ax=None, lum_params=None, color_by_ang_class=None,
                 s=None, alpha=None):
        """Plot a rescaled dataset in the PCA space.

        Parameters:
        -----------
        axes : list of int, optional
            List of the number of axes that sould be plot. Indexation starts at
            1. Default to [1,2]
        title : string, optional
            Common name of the plot. The complete name will be title +
            'Axe {}{}' where {} is the axe number. Default to None.
        rgb_compo : list of int [R, G, B], (optional),
            List of the number of axes use for a RJGB composition. Indexation
            start at 1. Indicate -1 if the mean lum of the windows is to be
            use. If not indicated, dot will appear in black. Default to None.
            Should no be indicated if angular_colors is.
        score = string {`raw`,`std`} (optional)
            If raw scores or standardized scores are plot. Default to
            Foto_dispayer.foto.score_mode.
        angular_colors : int (optional)
            If indicated, score will be classified according to their angle
            from the abscissa axis among a number of `angular_colors` clusters.
            Should not be indicated if rgb_compo is.
        plot_marker : bool (optional)
            If true, marker corresponding to number from 1 to 'angular_colors'
            will be plot instead of point. Should be indicated if
            angulars_colors is. Default to False.
        color_by_ang_class TODO
        s
        alph = None
        save : bool, (optional)
            If true the plot will be saved, with predetermined name
        out_filename : str (optional)
            Path of output plot.
        ax : `matplotlib.axes._subplots.AxesSubplot` instance (optional)
            If indicated, plot will be drawn in axe provided. Else en new
            matplotlib figure is created.

        **lum_params** a dictionnary whose keys are :

        classes_mode : {`percentile`, `absolute`},
            Mean luminance classes can be define by percentile or by
            abolute value.
        classes_th : tuple of flaat (lim_inf, lum_sip),
            Limits of Luminance classe expressend in percentile or absolute
            value depending on the `lum_classes_mode` chosen. E.g.
            ``(0,33)`` for percentiles.
        """
        msg_lum = False
        score = score or self.foto.score_mode
        if out_filename:
            filename = out_filename
        elif save:
            if rgb_compo:
                str_rgb = ''.join([str(i) for i in rgb_compo])
                add = '_rgb{}'.format(str_rgb)
            elif angular_colors:
                add = '_{}angcluster'.format(angular_colors)
            else:
                add = ''
            ax_str = ''
            for a in axes:
                ax_str += str(a)
            suffix = 'PCAplan_axes{}{}_{}.png'.format(ax_str, add, score)
            filename = join(self.foto.output_fold, suffix)
        else:
            filename = False

        if rgb_compo:
            if -1 in rgb_compo:
                self.foto.in_img.veg_lum._print_lum_msg()
                msg_lum = True
            color_table = self.get_color_table(axes=rgb_compo, score=score)
            tab_markers = None
        elif angular_colors:
            jet = plt.get_cmap('jet')
            cNorm = colors.Normalize(vmin=0, vmax=angular_colors)
            scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)
            color_table = [scalarMap.to_rgba(i) for i in range(angular_colors)]
            tab_markers = self.foto._get_angular_class(angular_colors,
                                                       score=score)
        else:
            color_table = None
            tab_markers = None

        if color_by_ang_class:
            color_table = color_by_ang_class
        # plot standardized or raw axes scores
        if score == 'raw':
            tab_score = self.foto.pca.coorditot
        elif score == 'std':
            tab_score = self.foto.pca.std_coorditot

        if -1 in axes:
            if not msg_lum:
                self.foto.in_img.veg_lum._print_lum_msg()

            mean_lum = self.foto.mean_lum_by_window
            if self.foto.in_img.veg_lum.veg_color == 'dark':
                msg = ('Sign of mean grey level is changed since ' +
                       'vegeation appears as dark in original input image')
                print msg
                mean_lum = [- mean_l for mean_l in mean_lum]

            mean_lum = sp.asarray(mean_lum)
            mean_lum = mean_lum.reshape((mean_lum.shape[0], 1))
            data = sp.concatenate((tab_score, mean_lum), axis=1)
        else:
            data = tab_score

        if lum_params is not None:
            classes_mode = lum_params['classes_mode']
            classes_th = [lum_params['classes_th']]
            sel_by_lum, _ = self.foto.select_win_id_by_mean_lum(classes_mode,
                                                                classes_th)
            sel_by_lum = sel_by_lum[0]
            data = data[sel_by_lum]
            tab_markers = (tab_markers[sel_by_lum] if tab_markers is not None
                else None)

        return plot_pca(data, eigval=self.foto.pca.vp, axes=axes, title=title,
                        out_filename=filename, color=color_table,
                        tab_markers=tab_markers, ax=ax, s=s, alpha=alpha,
                        plot_marker=plot_marker)

    def polar_plan(self, axes=[1, 2], title='PCA plan of', label=False,
                   rgb_compo=None, save=False, score=None,
                   angular_colors=None, out_filename=None, ax=None,
                   lum_params=None, color_by_ang_class=None, s=None, alpha=None,
                   plot_ang_limit=None, theta_offset=None,
                   plot_ang_limit_label=None):

        """Plot a rescaled dataset in the PCA space.

        Parameters:
        -----------
        axes : list of int, optional
            List of the number of axes that sould be plot. Indexation starts at
            1. Default to [1,2]
        title : string, optional
            Common name of the plot. The complete name will be title +
            'Axe {}{}' where {} is the axe number. Default to None.
        rgb_compo : list of int [R, G, B], (optional),
            List of the number of axes use for a RJGB composition. Indexation
            start at 1. Indicate -1 if the mean lum of the windows is to be
            use. If not indicated, dot will appear in black. Default to None.
            Should no be indicated if angular_colors is.
        score = string {`raw`,`std`} (optional)
            If raw scores or standardized scores are plot. Default to
            Foto_dispayer.foto.score_mode.
        angular_colors : int (optional)
            If indicated, score will be classified according to their angle
            from the abscissa axis among a number of `angular_colors` clusters.
            Should not be indicated if rgb_compo is.
        color_by_ang_class TODO
        s
        alph = None
        save : bool, (optional)
            If true the plot will be saved, with predetermined name
        out_filename : str (optional)
            Path of output plot.
        ax : `matplotlib.axes._subplots.AxesSubplot` instance (optional)
            If indicated, plot will be drawn in axe provided. Else en new
            matplotlib figure is created.

        **lum_params** a dictionnary whose keys are :

        classes_mode : {`percentile`, `absolute`},
            Mean luminance classes can be define by percentile or by
            abolute value.
        classes_th : tuple of flaat (lim_inf, lum_sip),
            Limits of Luminance classe expressend in percentile or absolute
            value depending on the `lum_classes_mode` chosen. E.g.
            ``(0,33)`` for percentiles.
        """

        msg_lum = False
        score = score or self.foto.score_mode

        if out_filename:
            filename = out_filename
        elif save:
            if rgb_compo:
                str_rgb = ''.join([str(i) for i in rgb_compo])
                add = '_rgb{}'.format(str_rgb)
            elif angular_colors:
                add = '_{}angcluster'.format(angular_colors)
            else:
                add = ''
            ax_str = ''
            for a in axes:
                ax_str += str(a)
            suffix = 'Polarplan_axes{}{}_{}.png'.format(ax_str, add, score)
            filename = join(self.foto.output_fold, suffix)
        else:
            filename = False

        if rgb_compo:
            if -1 in rgb_compo:
                self.foto.in_img.veg_lum._print_lum_msg()
                msg_lum = True
            color_table = self.get_color_table(axes=rgb_compo, score=score)
            tab_markers = None
        elif angular_colors:
            jet = plt.get_cmap('jet')
            cNorm = colors.Normalize(vmin=0, vmax=angular_colors)
            scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)
            color_table = [scalarMap.to_rgba(i) for i in range(angular_colors)]
            tab_markers = self.foto._get_angular_class(angular_colors,
                                                       score=score)
        else:
            color_table = None
            tab_markers = None

        if color_by_ang_class:
            color_table = color_by_ang_class

        # plot standardized or raw axes scores
        if score == 'raw':
            tab_score = self.foto.pol_coord(theta_offset=theta_offset)
        elif score == 'std':
            tab_score = self.foto.pol_coord(score='std',
                                            theta_offset=theta_offset)

        theta = tab_score['theta'].reshape(-1, 1)
        r = tab_score['r'].reshape(-1, 1)
        tab_score = sp.concatenate((theta, r), axis=1)

        if -1 in axes:
            if not msg_lum:
                self.foto.in_img.veg_lum._print_lum_msg()

            mean_lum = self.foto.mean_lum_by_window
            if self.foto.in_img.veg_lum.veg_color == 'dark':
                msg = ('Sign of mean grey level is changed since ' +
                       'vegeation appears as dark in original input image')
                print msg
                mean_lum = [- mean_l for mean_l in mean_lum]

            mean_lum = sp.asarray(mean_lum)
            mean_lum = mean_lum.reshape((mean_lum.shape[0], 1))
            data = sp.concatenate((tab_score, mean_lum), axis=1)
        else:
            data = tab_score

        if lum_params is not None:
            classes_mode = lum_params['classes_mode']
            classes_th = [lum_params['classes_th']]
            sel_by_lum, _ = self.foto.select_win_id_by_mean_lum(classes_mode,
                                                                classes_th)
            sel_by_lum = sel_by_lum[0]
            data = data[sel_by_lum]
            tab_markers = (tab_markers[sel_by_lum] if tab_markers is not None
                else None)

        return plot_polar_plan(data, axes=axes, title=title,
                        out_filename=filename, color=color_table,
                        tab_markers=tab_markers, ax=ax, s=s, alpha=alpha,
                        plot_ang_limit=plot_ang_limit,
                        plot_ang_limit_label=plot_ang_limit_label,
                        n_ang_class=angular_colors,)

    def pca_loading(self, ax=None, norm_x=True, axes=[1, 2, 3], save=False,
                    mode='curve', out_filename=None, axe_label='auto',
                    num_angulars_class_lim=None):
        """
        Plot PCA loading as function of spatial frequencies in km-1.

        Parameters
        ----------
        norm_x : bool, (optional)
            If True, the x axis scale will be set to log-scale. Default to True
        axis : list, (optional)
            List of the princpal component index to plot. Indexation stats at
            1. Default to the 3 first components.
        save : bool, (optional)
            If true the plot will be saved, with predetermined name
        out_filename : str (optional)
            Path of output plot.
        mode : str ``<'fill', 'curve'>`` (optional)
            If loading are fill or not under the curve.
        ax : `matplotlib.axes._subplots.AxesSubplot` instance (optional)
            If indicated, plot will be drawn in axe provided. Else en new
            matplotlib figure is created.
        axe_label : tuple (x_label, y_label) (optional)
            x and y Label. Defautl label are used if set to "auto".
        num_angulars_class_lim : int (optional)
            If indicated the limits of num_angulars_class_lim will be drawn.
        """
        LIST_MODE = ('fill', 'curve')
        f_km = self.foto.cycles_km
        labels = ['1st axis', '2nd axis', '3rd axis']
        load = self.foto.pca.loading
        color = ['crimson', 'lime', 'dodgerblue']

        if ax:
            ax = ax
            subplot = True
        else:
            ax = plt.subplots()[1]
            subplot = False
        #ax = ax or plt.subplots()[1]
        #plt.figure()
        if norm_x:
            ax.set_xscale('log')
        for col, num_ax in zip(color, axes):
            if mode not in LIST_MODE:
                msg = "'mode' parameters has to be one of these choices {}"
                raise ValueError(msg.format(LIST_MODE))
            ax.plot(f_km, load[:, num_ax - 1].transpose(),
                     label=labels[num_ax - 1], color=col)
            if mode == 'fill':
                ax.fill_between(f_km, 0, load[:, num_ax - 1].transpose(),
                                label=None, alpha=0.5,
                                facecolor=col, edgecolor='gainsboro')

        ax.legend(loc=0,  fontsize='small')
        ax.axhline(0, color="gray", ls='dashed')
        if num_angulars_class_lim:
            lim_classes = self.foto.frequencies_by_ang_class(
                classes=num_angulars_class_lim)
            lim_classes = [ran.min() for ran in lim_classes]
            lim_classes.remove(min(lim_classes))
            lim_classes.remove(max(lim_classes))
            for lim in lim_classes:
                ax.axvline(lim, color="gray", ls='-', linewidth=1)
        AXE_LABEL_DEFAULT = ('Spatial frequencies (km-1)',
                             'Scaled PCA loading')
        _plot_axe_label(axe_label, AXE_LABEL_DEFAULT, ax)
#        ax.xlabel('Spatial frequencies (km-1)')
#        ax.ylabel('Scaled PCA loading')
        # ax.set_title('')
        if out_filename:
            plt.savefig(out_filename)
        elif save:
            suffix = 'pca_loading.png'
            plt.savefig(join(self.foto.output_fold, suffix), format='png')

        if subplot:
            return ax
        else:
            plt.show()

    def _get_streched_min_max(self):
        """
        """
        try:
            vmin = self.foto.in_img.get_percentile(2, idx=1)
            vmax = self.foto.in_img.get_percentile(98, idx=1)
        except KeyError:
            _ = self.foto.in_img.compute_percentile([2, 98])
            vmin = self.foto.in_img.get_percentile(2, idx=1)
            vmax = self.foto.in_img.get_percentile(98, idx=1)
        return vmin, vmax

    def plot_angular_extems_windows(self, **kwargs):
        """
        Plot a list of figures containing windows whose distance to the center
        of the PCA plan is maximal, by angular sector and by mean luminance
        range. A figure for each combination of angular sector and luminance
        range is plotted.

        Parameters
        ----------
        *Classes defination*
            classes : list of angles ``[[ang_min,ang_max], [,],...]`` or int,
            optional
                List of the radial classes, expressed in positive radian. A
                single value can also be indicated. In the case where an
                integer is indicated, 'classes' equal angular classes will be
                created. For instance if ``'classes' = 8``, 8 classes of pi/4
                rad are created. By default, 8 classes are created, the first
                is [0,pi/4].
            lum_classes_mode : {`False`, `percentile`, `absolute`}, (optional)
                If windows must be selected according to their mean luminance.
                Mean luminance classes can be define by percentile or by
                abolute value. If False, windows will be selected by angular
                sector only, whatever their mean luminance value. False by
                default.
            lum_classes_th : list of tupe of flaat, (optional)
                List of luminance classe expressend in percentile or absolute
                value depending on the `lum_classes_mode` chosen. E.g.
                ``[[0,33], [33,66],...]`` for percentiles.
            n_w : int, (optional)
                Number of windows to select. Should not be indicated if thresh
                is indicated.
            thresh: int, (optional)
                Distance thresh from the center of the PCA plan - expressed in
                percentage of maximum distance- beyond which windows are
                selected. Should not be indicated if n_w is indicated.
            classes_to_show : list of int, (optional)
                Idexes of the classes to plot. If not indicated all, the
                classes will be plot
            score : string {`raw`, `std`} (optional)
                If raw scores or standardized scores are taking into account
                for classe definition. Default to
                Foto_dispayer.foto.score_mode.
            r_max : int, betwee 0 and 100 (optional)
                Windows with a distance to the center greater than r_max
                won't be consider for the selection. R_max is expressed in
                percentile and compute for each angular classif. By default
                r_max is inf.

        *Plot parameters*
            plot_structure : tuple of int (nrow, ncol)
                Number of columns and row for the subplot
            figsize : tuple of int, (optional)
                Control the size of windows, see matplolib.pyplot.figure
                documentation for more details
            save : bool, (optional)
                If true the plot will be saved, with predetermined name
            out_filename : str (optional)
                Path of output plot.
            stretch : bool, (optional)
                If true, windows colors will be strech to percentile 2 and 98
                (only available for one band image).
            plot_window_id : bool, (optional)
                If True, windows id are plotted. True by default.
            adjust_plot_space : bool, (optional)
                If True, space between subplot will be reduced. False by
                default.
            plot_title : bool, (optional)
                If False, no title will be plotted. True by default.

        """

        # get classes boundaries, windows and their id
        classes = kwargs.get('classes')
        thresh = kwargs.get('thresh')
        n_w = kwargs.get('n_w')
        n_cla = len(classes) if isinstance(classes, list) else classes
        score = kwargs.get('score') or self.foto.score_mode

        dict_subplot = self.foto.get_extrem_angular_window(
            classes=classes, tresh=thresh, n_w=n_w, score=score,
            lum_classes_mode=kwargs.get('lum_classes_mode'),
            lum_classes_th=kwargs.get('lum_classes_th'),
            r_max=kwargs.get('r_max'))

        # definine/get plot param
        nrows, ncols = kwargs.get('plot_structure')
        classes_to_show = kwargs.get('classes_to_show', range(n_cla))

        # stretch grey level values
        if kwargs.get('stretch') and self.foto.in_img.count == 1:
            vmin, vmax = self._get_streched_min_max()
        else:
            vmin = None
            vmax = None

        # for each lum classe
        for num_lum, lum_label in enumerate(dict_subplot['lum_labels']):
            # for each class, the list of corresponding windows id and windows
            class_iterator = enumerate(zip(
                dict_subplot['windows'][lum_label],
                dict_subplot['classes'],
                dict_subplot['windows_id'][lum_label]))
            for i, (sub_plots, (deg_inf, deg_sup), list_id) in class_iterator:

                if i not in classes_to_show:
                    continue
                fig, axes = plt.subplots(nrows=nrows,
                                         figsize=kwargs.get('figsize'),
                                         ncols=ncols, sharex=True, sharey=True)

                if kwargs.get('plot_title', True):
                    title = ('Luminance class : {}\nAngular class : from {}' +
                             'to {} degrees').format(lum_label, deg_inf,
                                                     deg_sup)
                    fig.suptitle(title)

                # for each window, the corresponding location on sublot and id
                window_iterator = zip(sub_plots,
                                      product(range(nrows), range(ncols)),
                                      list_id)
                for tab, (r, c), (win_id) in window_iterator:
                    axes[r, c].imshow(tab, cmap=cm.Greys_r, vmin=vmin,
                                      vmax=vmax)
                    axes[r, c].axis('off')
                    grid_id = self.foto.grid_id_dict['id_grid'][win_id]
                    if kwargs.get('plot_window_id', True):
                        axes[r, c].set_title('id : {}'.format(grid_id),
                                             fontsize=8)

                if kwargs.get('adjust_plot_space'):
                    plt.subplots_adjust(wspace=0.02, hspace=0.02)
                plt.show()

                if kwargs.get('out_filename'):
                    plt.savefig(kwargs.get('out_filename'), format='png',
                                bbox_inches='tight')
                elif kwargs.get('save') is True:
                    _, filename = os.path.split(self.foto.in_img.filename)
                    suffix = ('extrem_windows_from_{}_' +
                              '{}_{}_to_{}deg_lum_{}_n{}').format(filename,
                                                                  score,
                                                                  deg_inf,
                                                                  deg_sup,
                                                                  num_lum,
                                                                  n_w)
                    out_name = join(self.foto.output_fold, suffix)
                    out_fmt = kwargs.get('out_fmt', 'png')
                    out_name += '.' + out_fmt
                    plt.savefig(out_name, format=out_fmt, bbox_inches='tight',
                                transparent=True)
        return dict_subplot['windows_id']

    def plot_angular_extems_windows_by_lum(self, **kwargs):
        """
        Plot a list of figures containing windows whose distance to the center
        of the PCA plan is maximal, by angular sector and by mean luminance
        range. A figure for each angular sector is plotted, each row
        corresponding to luminance range.

        Parameters
        ----------
        *Classes defination*
            classes : list of angles ``[[ang_min,ang_max], [,],...]`` or int,
            optional
                List of the radial classes, expressed in positive radian. A
                single value can also be indicated. In the case where an
                integer is indicated, 'classes' equal angular classes will be
                created. For instance if ``'classes' = 8``, 8 classes of pi/4
                rad are created. By default, 8 classes are created, the first
                is [0,pi/4].
            lum_classes_mode : {`False`, `percentile`, `absolute`}, (optional)
                If windows must be selected according to their mean luminance.
                Mean luminance classes can be define by percentile or by
                abolute value. If False, windows will be selected by angular
                sector only, whatever their mean luminance value. False by
                default.
            lum_classes_th : list of tupe of flaat, (optional)
                List of luminance classe expressend in percentile or absolute
                value depending on the `lum_classes_mode` chosen. E.g.
                ``[[0,33], [33,66],...]`` for percentiles.
            n_w : int, (optional)
                Number of windows to select. Should not be indicated if thresh
                is indicated.
            thresh: int, (optional)
                Distance thresh from the center of the PCA plan - expressed in
                percentage of maximum distance- beyond which windows are
                selected. Should not be indicated if n_w is indicated.
            classes_to_show : list of int, (optional)
                Idexes of the classes to plot. If not indicated all, the
                classes will be plot
            score : string {`raw`, `std`} (optional)
                If raw scores or standardized scores are taking into account
                for classe definition. Default to
                Foto_dispayer.foto.score_mode.
            r_max : int, betwee 0 and 100 (optional)
                Windows with a distance to the center greater than r_max
                won't be consider for the selection. R_max is expressed in
                percentile and compute for each angular classif. By default
                r_max is inf.
        *Plot parameters*
            figsize : tuple of int, (optional)
                Control the size of windows, see matplolib.pyplot.figure
                documentation for more details
            save : bool, (optional)
                If true the plot will be saved, with predetermined name
            out_filename : str (optional)
                Path of output plot.
            stretch : bool, (optional)
                If true, windows colors will be strech to percentile 2 and 98
                (only available for one band image).
            plot_window_id : bool, (optional)
                If True, windows id are plotted. True by default.
            adjust_plot_space : bool, (optional)
                If True, space between subplot will be reduced. False by
                default.
            plot_title : bool, (optional)
                If False, no title will be plotted. True by default.
        """

        # get classes boundaries, windows and their id
        classes = kwargs.get('classes')
        thresh = kwargs.get('thresh')
        n_w = kwargs.get('n_w')
        n_cla = len(classes) if isinstance(classes, list) else classes
        score = kwargs.get('score') or self.foto.score_mode
        dict_subplot = self.foto.get_extrem_angular_window(
            classes=classes, tresh=thresh, n_w=n_w, score=score,
            lum_classes_mode=kwargs.get('lum_classes_mode'),
            lum_classes_th=kwargs.get('lum_classes_th'),
            r_max=kwargs.get('r_max'))

        # definine/get plot param
        n_lum_class = (len(kwargs.get('lum_classes_th'))
            if kwargs.get('lum_classes_th') else 1)
        nrows, ncols = (n_lum_class, n_w)
        classes_to_show = kwargs.get('classes_to_show', range(n_cla))

        # stretch grey level values
        if kwargs.get('stretch') and self.foto.in_img.count == 1:
            vmin, vmax = self._get_streched_min_max()
        else:
            vmin = None
            vmax = None

        # for each angular classes' limit
        class_iterator = enumerate(dict_subplot['classes'])
        for i, (deg_inf, deg_sup) in class_iterator:
            if i not in classes_to_show:
                continue
            fig, axes = plt.subplots(nrows=nrows,
                                     figsize=kwargs.get('figsize'),
                                     ncols=ncols, sharex=True, sharey=True)
            if kwargs.get('plot_title', True):
                title = ('Angular class : from {}' +
                         'to {} degrees').format(deg_inf,
                                                 deg_sup)
                fig.suptitle(title)

            # for each luminance class
            for num_lum, lum_label in enumerate(dict_subplot['lum_labels']):
                sub_plots = dict_subplot['windows'][lum_label][i]
                list_id = dict_subplot['windows_id'][lum_label][i]

                # for each windows of a given luminance and angular sector
                window_iterator = enumerate(zip(sub_plots,
                                                list_id))
                for col_idx, (tab, win_id) in window_iterator:
                    axes[num_lum, col_idx].imshow(tab, cmap=cm.Greys_r,
                                                  vmin=vmin, vmax=vmax)

                    # Hide axes ticks
                    axes[num_lum, col_idx].set_xticks([])
                    axes[num_lum, col_idx].set_yticks([])
                    axes[num_lum, col_idx].spines['bottom'].set_color('white')
                    axes[num_lum, col_idx].spines['top'].set_color('white')
                    axes[num_lum, col_idx].spines['right'].set_color('white')
                    axes[num_lum, col_idx].spines['left'].set_color('white')

                    if kwargs.get('plot_window_id', True):
                        grid_id = self.foto.grid_id_dict['id_grid'][win_id]
                        axes[num_lum, col_idx].set_title(
                            'id : {}'.format(grid_id), fontsize=8)
                axes[num_lum, 0].set_ylabel('mean NDVI\n' + lum_label)

            if kwargs.get('adjust_plot_space'):
                plt.subplots_adjust(wspace=0.02, hspace=0.02)
            plt.show()

            if kwargs.get('out_filename'):
                plt.savefig(kwargs.get('out_filename'), format='png',
                            bbox_inches='tight')
            elif kwargs.get('save') is True:
                _, filename = os.path.split(self.foto.in_img.filename)
                suffix = ('extrem_windows_from_{}_' +
                          '{}_{}_to_{}deg_by_lum.png').format(filename, score,
                                                              deg_inf, deg_sup)
                out_name = join(self.foto.output_fold, suffix)
                plt.savefig(out_name, format='png', bbox_inches='tight',
                            transparent=True)

    def variance_explained(self, ax=None, n_axis='all', axe_label='auto',
                           legend='Cumulative sum', show_var_label=True):
        """
        Plot bar of variance and cumulative variances explained by first axes.

        Paremeters
        ----------
        n_axis : int or str,
            If "all", all axes are drawn, if "int" only the n_axis first.
        legend : str (optional)
            Legend.
        ax : `matplotlib.axes._subplots.AxesSubplot` instance (optional)
            If indicated, plot will be drawn in axe provided. Else en new
            matplotlib figure is created.
        axe_label : tuple (x_label, y_label) (optional)
            x and y Label. Defautl label are used if set to "auto".
        show_var_label : Bool (optional)
            If True, text values of explained variance for the three first axes
            are drawn.
        """

        self.foto.pca.compute_var_explained()
        var = sp.diag(self.foto.pca.var_explained) * 100
        if n_axis != 'all':
            try:
                var = var[:n_axis]
            except IndexError:
                msg = ('Integer value is expected for `n_axis` ' +
                       'not : {}'.format(n_axis))
                raise TypeError(msg)
        else:
            n_axis = var.shape[0]
        var_cum_sum = sp.cumsum(var)

        if ax:
            ax = ax
            subplot = True
        else:
            ax = plt.subplots()[1]
            subplot = False
        # ax = ax or plt.subplots(tight_layout=True)[1]

        x = range(n_axis)
        ax.bar(x, var, 0.8, align='center',)
        ax.plot(x, var_cum_sum, color='crimson', ls='dashed',
                label=legend)

        if show_var_label:
            for v, x_pos in zip(var[:3], x):
                ax.text(x_pos + 0.3, v, '{:.1f}'.format(v))

        AXE_LABEL_DEFAULT = ('Axes', 'Variance explained (in %)')
        _plot_axe_label(axe_label, AXE_LABEL_DEFAULT, ax)

        if legend:
            ax.legend(loc=0)
        if subplot:
            return ax
        else:
            plt.show()

    def spectre_vs_spectre(self, foto,id_win = None, xlab = None, ylab = None):
        """
        """

        dif = self.nb_f - foto.nb_f
        if dif > 0 :
            spectre_1 = self.sp_norm_by_winvar()[:,:-dif]
            spectre_2 = foto.sp_norm_by_winvar()
        elif dif < 0:
            spectre_1 = self.sp_norm_by_winvar()
            spectre_2 = foto.sp_norm_by_winvar()[:,:dif]
        else:
            spectre_1 = self.sp_norm_by_winvar()
            spectre_2 = foto.sp_norm_by_winvar()

        fig = plt.figure()
        plt.xscale('log')
        plt.yscale('log')
        if id_win is not None:
            plt.plot(spectre_1[id_win,:], spectre_2[id_win,:], '.b')
            mi = min(spectre_1[id_win,:].min(),spectre_2[id_win,:].min())
            ma = max(spectre_1[id_win,:].max(),spectre_2[id_win,:].max())
            plt.title('Correlation of portion of windows variance num {},\nwindows size : {}  '.format(id_win, self.w_size * self.p_size))
        else:
            plt.plot(spectre_1[:,:], spectre_2[:,:], '.b')
            mi = min(spectre_1[:,:].min(),spectre_2[:,:].min())
            ma = max(spectre_1[:,:].max(),spectre_2[:,:].max())
            plt.title('Correlation of portion of windows variance,\n windows size : {} '.format(self.w_size * self.p_size))

        plt.plot([mi,ma],[mi,ma],'k')
        plt.xlabel(xlab)
        plt.ylabel(ylab)

    def r_spectra(self, r_spec, norm_x = True,
                     norm_y = True, label = None):
        """
        TODO
        """
        if len(r_spec) <= 9:
            colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k',
                      '0.75', '#ff9a4e'][:len(r_spec)]
        else:
            colors = sp.rand(len(r_spec), 3)
        if label is None:
            label = [None for i in r_spec]

        f_km = self.foto.cycles_km
        fig, ax = plt.subplots()

        # plot the r-spectra
        for lab ,spc, col in zip(label, r_spec, colors):
            ax.plot(f_km, spc, label=lab, color=col)
        ax.legend(loc=0,  fontsize='small')
        if norm_y :
            ax.set_yscale('log')
            #ymin, ymax = ax.ylim()
            #plt.ylim(0.0001, 100)
        if norm_x:
            ax.set_xscale('log')
        ax.set_ylabel('Portion of variance')
        ax.set_xlabel('Frequency (km-1)')
        plt.show()

    def color_vs_freq(self, axes=[1, 2, 3], xlim = None, ndvi_val=None,
                      **kwargs):
        """
        Plot the RGB colors composition for each frequence. The values of R,G,B
        respectively correspond to the loading of axis 1,2 and 3. Frequencies are
        expressed in km-1.

        Parameters
        ----------
        xlim : tuple ``(xmin,xmax)`` (optional)
             Min and max of the x_axis
        n_ticks : int (optional)
            Number of ticks to indicate
        """
        f_km = self.foto.cycles_km
        load = self.foto.pca.loading
        color = ['crimson', 'lime', 'dodgerblue']

        axes = [a - 1 if a != -1 else -1 for a in axes]
        #rescale from 0 to 1 the loadings (necessary for the RGB plotting)
        r_load = lib.rescale_data(load)

        #frequencies distribution is better understandable with log scale
        #but interpolation must be done.

        f_km = sp.log10(sp.asarray(f_km))  #log the frequencies
        new_freq = sp.linspace(f_km.min(), f_km.max(), 200)   #resample the freq
        new_r_load = r_load.copy()
        list_cols = []
        for a in axes:
            if a == - 1:
                list_cols.append(sp.ones((200, 1), dtype='float' ) * ndvi_val )
                continue
            list_cols.append(sp.interp(new_freq, f_km,
                                  new_r_load[:,a]).reshape(-1,1))
        new_r_load = sp.concatenate(list_cols, axis=1)
        #----Reshaping for propper plotting
        #new_r_load = sp.transpose(new_r_load)
        new_r_load = new_r_load.reshape((new_r_load.shape[0],1,3))
        new_r_load = new_r_load.reshape((1,new_r_load.shape[0],3))

        #----Plot
        _, ax = plt.subplots()
        extent = [10**f_km.min(),10**f_km.max(), 0,2]
        ax.imshow(new_r_load, extent = extent)
        ax.set_xscale('log')
        xmin, xmax = xlim if xlim else (f_km.min(),f_km.max())
        #Define fixed number of ticks at fixed location
        ticks =  sp.round_(sp.logspace(xmin,xmax,kwargs.get('n_ticks', 10)))
        ax.set_xticks(ticks)
        ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.spines["left"].set_visible(False)
