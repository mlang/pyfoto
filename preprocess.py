# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 15:03:28 2018

@author: marc
"""
import os
from ymraster import Raster, write_file
import scipy as sp
import pandas as pd
#import generic.extract_information as ei
import pyfoto.lib as lib

def reshape_img_in_strip(img_file, w_size, out_strip_img, mode='auto',
                         out_coord=None, in_coord=None,
                         nodata=None, idx_band=1,
                         r_offset=0, c_offset=0):
    """
    Reshape an image in a single strip.
    The original image is tiled according to a grid of square cells of size
    `w_size`. Each window (cell) is then reordered in single strip image of
    height of `w_size`. If the last row or column of the grid contains less
    pixels than `w_size`, they are not take into account.

    Parameters
    ----------
    img_file : string,
        Path of the image to resamp.
    w_size : int,
        Size of the windows to consider (in pixels). In case `w_size` is even,
        the actual windows size will be `w_size` - 1 because of FOTO limitations.
        However the slicing of the original image will effectively be done by
        square of `w_size`. E.g. for a `w_size` of 50, the first windows is
        extracted from pixel 1 to 49 on the original image, and the second one
        from 51 to 99 etc.
    mode : string {`auto`, `from_coord`}
        Define the way tiles order are definend :

            * `auto` : the image is tiled automatically and a coord file is saved
            * `from_coord` : the image is tiled according to a provided list of tiles

    out_coord : string, **(for mode `auto`)**
        Path of the binary file (.npy) containing a list of the index of the top
        left pixel of tiles that not contains any nodata pixels.
    in_coord : numpy.ndarray (n, 2), **(for mode `from_coord`)**
        Array containing a list of the index of the top
        left pixel of tiles that not contains any nodata pixels.
    out_strip_img : srting,
        Path of the output reshaped image. Can be any format supported by Gdal.
    idx_band : int,
        Index of band to resample. Indexation starts to 1. Default to 1.
    nodata : float,(optional)
        Nodata value. Windows that contains nodata values will not be resamp in
        the output img. By default, `nodata` = `None`
    r_offset : int
        The top left origin of the grid will be 0,0 if r_offset is not
        indicated. If indicated it will be r_offset, c_offset.
    c_offset : int
        The top left origin of the grid will be 0,0 if r_offset is not
        indicated. If indicated it will be r_offset, c_offset.

    Limitations
    -----------
    The function currently supports only square windows. Only one band can be
    resampled.
    """

    if mode == 'auto':
        coord = create_coord_file(img_file, w_size, out_coord,
                                  nodata=nodata, idx_band=idx_band,
                                  r_offset=r_offset, c_offset=c_offset)
    elif mode == 'from_coord':
        coord = in_coord

    _reshape_img_in_strip(img_file, w_size, out_strip_img,
                          coord, idx_band=1)

    # Conversion in ers format to FOTO processing
    if os.path.splitext(out_strip_img)[1] == '.ers':
        _update_ers_metadata(out_strip_img)


def _reshape_img_in_strip(img_file, w_size, out_strip_img,
                          coord, idx_band=1):
    """
    Create a strip image from the list of window coordinate in the original
    image.

    Parameters
    ----------
    img_file : string,
        Path of the image to resamp.
    w_size : int,
        Size of the windows to consider (in pixels). In case `w_size` is even,
        the actual windows size will be `w_size` - 1 because of FOTO limitations.
        However the slicing of the original image will effectively be done by
        square of `w_size`. E.g. for a `w_size` of 50, the first windows is
        extracted from pixel 1 to 49 on the original image, and the second one
        from 51 to 99 etc.
    out_strip_img : srting,
        Path of the output reshaped image. Can be any format supported by Gdal.
    coord : numpy.ndarray (n, 2), **(for mode `from_coord`)**
        Array containing a list of the index of the top
        left pixel of tiles that not contains any nodata pixels.
    idx_band : int,
        Index of band to resample. Indexation starts to 1. Default to 1.
    """

    img = Raster(img_file)
    w_size_real = w_size - 1 if w_size % 2 == 0 else w_size

    # Create the strip image
    resample_img = sp.empty((w_size_real, len(coord) * (w_size_real)),
                            dtype=img.dtype.numpy_dtype)

    # fill it with each tile index of coord.
    for i, [r, c] in enumerate(coord):
        col_begin = i * (w_size_real)
        col_end = i * (w_size_real) + w_size_real
        windows_tab = img.array_from_bands(idx_band, block_win=(c, r,
                                                                w_size_real,
                                                                w_size_real))
        resample_img[:, col_begin: col_end] = windows_tab

    # Save the strip image
    dtype = img.dtype
    write_file(out_filename=out_strip_img, array=resample_img, dtype=dtype)


def create_coord_file(img_file, w_size, out_coord,
                      nodata=None, idx_band=1,
                      r_offset=0, c_offset=0):
    """
    Reshape an image in a single strip.
    The original image is tiled according to a grid of square cells of size
    `w_size`. Each window (cell) is then reordered in single strip image of
    height of `w_size`. If the last row or column of the grid contains less
    pixels than `w_size`, they are not take into account.

    Parameters
    ----------
    img_file : string,
        Path of the image to resamp.
    w_size : int,
        Size of the windows to consider (in pixels). In case `w_size` is even,
        the actual windows size will be `w_size` - 1 because of FOTO limitations.
        However the slicing of the original image will effectively be done by
        square of `w_size`. E.g. for a `w_size` of 50, the first windows is
        extracted from pixel 1 to 49 on the original image, and the second one
        from 51 to 99 etc.
    out_coord : string, **(for mode `auto`)**
        Path of the binary file (.npy) containing a list of the index of the top
        left pixel of tiles that not contains any nodata pixels.
    nodata : float,(optional)
        Nodata value. Windows that contains nodata values will not be resamp in
        the output img. By default, `nodata` = `None`
    idx_band : int,
        Index of band to resample. Indexation starts to 1. Default to 1.
    r_offset : int
        The top left origin of the grid will be 0,0 if r_offset is not
        indicated. If indicated it will be r_offset, c_offset.
    c_offset : int
        The top left origin of the grid will be 0,0 if r_offset is not
        indicated. If indicated it will be r_offset, c_offset.

    """

    img = Raster(img_file)

    n_col = int(img.width / w_size)
    n_row = int(img.height / w_size)

    coord = []

    # Foto doesnt support even number for windows size ...
    w_size_real = w_size - 1 if w_size % 2 == 0 else w_size

    count_nodata_windows = 0
    # get the index of the top left pixel of tiles that not contains any
    # nodata pixels it takes into account the windows size indicated and
    # not the ones taken into account by foto
    for win in range(n_col * n_row):
        r = (win // n_col) * w_size + r_offset
        c = (win % n_col) * w_size + c_offset
        if r + w_size_real > img.height or c + w_size_real > img.width:
            continue
        # take all windows if no nodata value indicated
        if nodata is None:
            coord.append([r, c])
        # else check if there are nodata pixels in the tiles before referecing
        # the tiles index
        else:
            windows_tab = img.array_from_bands(idx_band,
                                               block_win=(c, r,
                                                          w_size_real,
                                                          w_size_real),
                                               mask_nodata=False)
            is_nodata = sp.where(windows_tab == nodata)
            if is_nodata[0].shape[0] == 0:  # i.e. if there is not nodata value
                coord.append([r, c])
            else:
                count_nodata_windows += 1

    if nodata:
        print(('{} windows with nodata ' +
               '= {} were found').format(count_nodata_windows, nodata))

    # save the coordinate in case of succeeding
    sp.save(out_coord, coord)

    return coord

def reshape_img_in_strip_point_centered(img_file, w_size, out_coord,
                                        out_strip_img, point_vector,
                                        id_field_name, buffer_size_px,
                                        ):

    """
    Reshape an image in a single strip, centered on points. If buffers overlap,
    the pixels in question are taken into account several times,
    The original image is tiled according to a grid of square cells of size
    `w_size`. Each window (cell) is then reordered in single strip image of
    height of `w_size`. If the last row or column of the grid contains less
    pixels than `w_size`, they are not take into account.

    Parameters
    ----------
    img_file : string,
        Path of the image to resamp.
    w_size : int,
        Size of the windows to consider (in pixels). In case `w_size` is even,
        the actual windows size will be `w_size` - 1 because of FOTO limitations.
        However the slicing of the original image will effectively be done by
        square of `w_size`. E.g. for a `w_size` of 50, the first windows is
        extracted from pixel 1 to 49 on the original image, and the second one
        from 51 to 99 etc.
    out_coord : string,
        Path of the binary file (.npy) containing a list of the index of the top
        left pixel of tiles that not contains any nodata pixels.
    out_strip_img : string,
        Path of the output reshaped image. Can be any format supported by Gdal.
    ext_ers : string,
        An image of ers format is also created whose name has the same prefixe
        than indicated in `out_strip_img` and a `ext_ers` suffix.
    buffer_size_px : int,
        Half-side ('radius') of the square buffer in pixel.
    point_vector : str,
        Shapefile with the points.
    field_name : str,
        Name of the field of point_vector contaning the id of the point.

    Limitations
    -----------
    The function currently supports only square windows
    """

    pix_coord, xy_coord = lib.get_pixel_coord_from_points(point_vector,
                                                         img_file,
                                                         id_field_name)

    img = Raster(img_file)

    if w_size >= buffer_size_px:
        n_col = 1
    else:
        # Number of rows and columns for each buffer
        n_col = 2 * buffer_size_px / w_size
    # Foto doesnt support even number for windows size ...
    w_size_real = w_size - 1 if w_size % 2 == 0 else w_size

    coord = []
    tile_coord = []
    id_grid = 1
    for pt, (r_pt, c_pt) in pix_coord.iteritems():
        # Upper left corner of the buffer
        # the point is not exactly in the center, but in the upper-left part
        originRow = r_pt - buffer_size_px + 1
        originCol = c_pt - buffer_size_px + 1

        if n_col > 1 :
            # Register all top left coord for a point
            for win in range(n_col**2):   # Number of windows
                r = originRow + (win // n_col) * w_size
                c = originCol + (win % n_col) * w_size
                coord.append([r, c])

                tile_coord.append([id_grid, pt, r, c])
                id_grid += 1
        else:
            coord.append([originRow, originCol])
            tile_coord.append([id_grid, pt, originRow, originCol])

    # save the coordinate in case of succeeding
    sp.save(out_coord, coord)
    columns = ['id_grid', 'id_point', 'row', 'col']
    df = pd.DataFrame(tile_coord, columns=columns)
    df.to_csv(path_or_buf=out_coord.split('.')[0] + '.csv', index=False)

    if True:
        # Create the strip image
        n_row = w_size_real
        n_col = len(coord) * w_size_real
        resample_img = sp.empty((n_row, n_col), dtype=img.dtype.numpy_dtype)
        for i, [r, c] in enumerate(coord):
            beg_col = i * (w_size_real)
            end_col = i * (w_size_real) + w_size_real
            resample_img[:, beg_col: end_col] = img.array_from_bands(
                1, block_win=(c, r, w_size_real, w_size_real))

        # Save the strip image
        dtype = img.dtype
        write_file(out_filename=out_strip_img, array=resample_img, dtype=dtype)

        # Conversion in ers format to FOTO processing
        if os.path.splitext(out_strip_img)[1] == '.ers':
            _update_ers_metadata(out_strip_img)


def _update_ers_metadata(ers_file):
    """
    Function to complete missing information on the metadata ers file that
    is created with GDAL.
    """

    f = open(ers_file, 'r+')
    lines = f.readlines()
    l = 0
    # we need to insert a portion of text before this line
    while lines[l] != '\tRasterInfo Begin\n':
        l = l + 1

    text_to_add = ('\tCoordinateSpace Begin\n' +
                   '\t\tDatum	= "RAW"\n' +
                   '\t\tProjection	= "RAW"\n' +
                   '\t\tCoordinateType	= EN\n' +
                   '\t\tUnits	= "METERS"\n' +
                   '\t\tRotation	= 0:0:0.0\n' +
                   '\tCoordinateSpace End\n')

    f.seek(0)  # set index to begin of file
    f.writelines(lines[0:l])
    f.write(text_to_add)
    f.writelines(lines[l:len(lines)])
    f.close()
