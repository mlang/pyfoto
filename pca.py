# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 18:21:59 2018

@author: marc
"""

import math
import scipy as sp
from copy import deepcopy
from lib import Param_loader

def matrix_rotation_2D(M, theta):
    """
    """
    # define rotation matrix
    theta = math.radians(theta)
    c, s = sp.cos(theta), sp.sin(theta)
    R = sp.array([c, s, -s, c]).reshape(2, 2)

    # apply rotation
    return sp.dot(M, R)


def _new_eigvalues_from_rotation_2D(pca, theta,):
    """

    """
    theta_rad = math.radians(theta)
    eigval_1 = pca.eigtab[0, 0]
    eigval_2 = pca.eigtab[1, 1]
    eigval_1_rotated = (math.cos(theta_rad)**2 * (eigval_1 - eigval_2) +
                        eigval_2)
    eigval_2_rotated = (math.cos(theta_rad)**2 * (eigval_2 - eigval_1) +
                        eigval_1)

    return eigval_1_rotated, eigval_2_rotated


def rotate_pca_2d(pca, theta):
    """
    """
    PARAM_TO_ROTATE = ['std_coorditot', 'loading']

    for param in PARAM_TO_ROTATE:
        if hasattr(pca, param):
            temp_param = getattr(pca, param)
            temp_param[:, :2] = matrix_rotation_2D(temp_param[:, :2], theta)
            setattr(pca, param, temp_param)

    pca.eigtab[0, 0], pca.eigtab[1, 1] = _new_eigvalues_from_rotation_2D(pca,
                                                                         theta)

    pca.coorditot = sp.dot(pca.std_coorditot, sp.sqrt(pca.eigtab))
    pca.cosv = pca.loading
    return pca

class Pca(Param_loader):
    """

    """
    _LIST_PARAM = ['coorditot','cosv','eigtab','vp','eigvec','ctri','ctrv',
                   'cos2i','normvec','centeredsp', 'spectreswima']

    def __init__(self, fold_data=None, n_process=1):
        """
        """

        Param_loader.__init__(self, fold_data, self._LIST_PARAM,
                              n_process=n_process)
        self.init_param()

#    def load(self, fold_data):
#        """
#        """
#
#        for param in LIST_PARAM:
#            setattr(self,param,_load_file(fold_data, PATTERN[param]))

#        self.coorditot = _load_file(fold_data, PATTERN['coorditot'])
#        self.cosv = _load_file(fold_data, PATTERN['cosv'])
#        self.eigtab = _load_file(fold_data, PATTERN['eigtab'])
#        self.var_explained = _load_file(fold_data, PATTERN['vp'])
#        self.eigvec = _load_file(fold_data, PATTERN['eigvec'])
#        self.ctri = _load_file(fold_data, PATTERN['ctri'])
#        self.ctrv = _load_file(fold_data, PATTERN['ctrv'])
#        self.cos2i = _load_fstd_spile(fold_data, PATTERN['cos2i'])
#        self.normvec = _load_file(fold_data, PATTERN['normvec'])

    def init_param(self,):
        """

        """
        self.r_spectra = self.spectreswima[:, 7:]
        self.compute_loading()
        self.compute_centeredsp()
        self.compute_std_sp()
        self.compute_coorditot()
        self.compute_std_coorditot()
        self.compute_var_explained()
        self.compute_representation_quality()

    def compute_loading(self):
        """
        Compute loading from eigen vector and eigenvalues.
        """
        self.loading = sp.dot(self.eigvec, sp.sqrt(self.eigtab))

    def compute_std_sp(self):
        """
        Compute standardized data from centered data
        TODO
        """
        self.std_sp = sp.dot(self.centeredsp, self.normvec)

    def compute_centeredsp(self):
        """
        Compute standardized data from centered data
        TODO
        """
        self.centeredsp = self.r_spectra - self.r_spectra.mean(axis=0)

    def compute_var_explained(self):
        """
        Compute variance explained by each component of the PCA
        """
        sum_eigval = float(self.eigtab.sum())
        self.var_explained = self.eigtab / sum_eigval

    def compute_coorditot(self):
        """
        Compute raw scores of data table, from the standardized data and
        the eigenvectors.
        """
        self.coorditot = sp.dot(self.std_sp, self.eigvec)

    def compute_std_coorditot(self):
        """
        Compute standardized scores, by removing to raw score their variance,
        i.e. by multplying raw score by the pseudo inverse eigen values
        matrices.
        """
        pseudo_inverserd_eigtab = sp.linalg.pinv(sp.sqrt(self.eigtab))
        self.std_coorditot = sp.dot(self.coorditot, pseudo_inverserd_eigtab)

    def compute_representation_quality(self):
        """
        Compute representation quality from PC raw scores
        """
        C = self.coorditot
        C2 = C * C
        C2_sumrow = C2.sum(axis=1)
        self.cos2i = C2 / C2_sumrow[:,None]


    def copy(self):
        """
        Return a deep copy of itself
        """
        return deepcopy(self)

    def rotation_2D(self, theta, in_place=False):
        """
        Apply a 2D rotation (rest of PC stay unchanged) and return a PCA
        instance with followings parameters updated :
            - coorditot
            - eigvec
            - cosv
            - loading

        Limitations
        -----------
        Rest of the parameters are not updated (ctrv for example).
        """
        if in_place:
            _ = rotate_pca_2d(self, theta)
        else:
            return rotate_pca_2d(self.copy(), theta)

    def eigtab_from_loading(self,):
        """
        """

        sum_squarred_loading = sp.square(self.loading).sum(0)
        return sp.diag(sum_squarred_loading)

#    def _subset_one_dim(self, attribute, n_component):
#        """
#        """
#        _LIST_PARAM = ['coorditot','cosv','eigtab','vp','eigvec','ctri','ctrv',
#                      'cos2i','normvec','centeredsp']
#
#
#        return getattr(pca, attribute)[:,:n_component]
#
#    def _subset_two_dim(self, attribute, n_component):
#        """
#        """
#        _LIST_PARAM = ['coorditot','cosv','eigtab','vp','eigvec','ctri','ctrv',
#                      'cos2i','normvec','centeredsp']
#
#        return getattr(pca, attribute)[:n_component,:n_component]
#
#    def subset(self, n_component):
#        """
#        """
#        _LIST_PARAM_ONE_DIM = ['coorditot','cosv','eigtab','vp','eigvec','ctri','ctrv',
#                      'cos2i','normvec','centeredsp']
#        _LIST_PARAM = ['coorditot','cosv','eigtab','vp','eigvec','ctri','ctrv',
#                      'cos2i','normvec','centeredsp']
#
#        sub_pca = self.copy()
#        sub_pca.