# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 18:06:35 2018

@author: marc
"""

import os
import math
from ymraster import Raster, RasterDataType, write_file
from pca import Pca
import scipy as sp
import pandas as pd
import lib
from lib import Param_loader
from lib import generate_id_from_coord
from lib import pol_coord_from_xy
from display import Foto_displayer
from os.path import join
from copy import deepcopy
from itertools import product

class VegetationLum(object):
    """
    Contain link between spectral band of an image and brightness of vegetation
    appears.

    Parameters
    ----------
    band : str (optional)
        Band type. If indicated VegetationLum.veg_color will automatically be
        set. Can be :
            - red
            - infrared
            - ndvi
            - panchro
    veg_color : str (optional)
        How vegetation appears on a band. Can be either `dark` or `bright`.
        Should not be indicated if band is.

    Attribute
    ---------
    veg_color : str,
        How vegetation appears on a band. Can be either `dark` or `bright`.
        Default value is `dark`.
    band : str,
        Band type. Default to unknown.
    """
    VEG_COLOR = ('dark', 'bright')

    BAND_TYPE = {'red': VEG_COLOR[0],
                 'infrared': VEG_COLOR[1],
                 'ndvi': VEG_COLOR[1],
                 'panchro': VEG_COLOR[0]}

    def __init__(self, veg_color=None, band=None):
        if band:
            if band not in self.BAND_TYPE.iterkeys():
                raise KeyError(('band type not supported, bands supported ' +
                                'are : {}'.format(self.BAND_TYPE.keys())))
            else:
                self.veg_color = self.BAND_TYPE[band]
                self.band = band
        elif veg_color:
            self.veg_color = veg_color
            self.band = 'unknown'
        else:
            self.veg_color = 'dark'
            self.band = 'unknown'

    @property
    def veg_color(self,):
        return self._veg_color

    @veg_color.setter
    def veg_color(self, veg_color):
        if veg_color not in self.VEG_COLOR:
            error_msg = "'veg_color' can only be on of these choice {}"
            raise ValueError(error_msg.format(self.VEG_COLOR))
        else:
            self._veg_color = veg_color

    def _print_lum_msg(self,):
        """
        Print message on current state of veg_color. Made to be called
        from composite class.
        """
        msg = ('Vegetation color is consider to be {0} in the inuput image' +
               '.Change Foto.in_img.veg_lum.veg_color' +
               ' if needed').format(self.veg_color)
        print msg


class InputRaster(Raster):
    """
    Input image of FOTO method from which many attributes on image information
    and method.

    Parameters
    ---------
    input_img : str
        Path of the FOTO input image.
    band : str (optional)
        Band type. If indicated VegetationLum.veg_color will automatically be
        set. Can be :
            - red
            - infrared
            - ndvi
            - panchro
    veg_color : str (optional)
        How vegetation appears on a band. Can be either `dark` or `bright`.
        Should not be indicated if band is.

    Attributes
    ----------
    veg_lum : `VegetationLum` instance
        Information on how vegetation appears (dark or bright) on the input
        image file.

    See Also
    --------
    ``Raster`` class from ymraster package
    """

    def __init__(self, input_img, veg_color=None, band=None):

        Raster.__init__(self, input_img)
        if not band:
            try:
                band = self.get_band_metadata(1)['BAND_TYPE']
            except KeyError:
                pass
        self.veg_lum = VegetationLum(veg_color=veg_color, band=band)


class ParamFoto(object):
    """
    Fold_name containing output of FOTO is expecting to have this format :
    {-7}_w{-6}_{-5}cm_{-4}_{-3}_{-2}_zpadf={-1} with :
        - 7 : Image Name
        - 6 : Window size wanted in output FMAP
        - 5 : Resolutiuon of image
        - 4 : Window size considered by FOTO
        - 3 : Moving window facto
        - 2 : Number of frequencies taken into account
        - 1 : Zero padding factor
    ParamFoto alows to extract automatically the input paramter of FOTO
    analysis according to the output foldname create by MATLAB FOTO programme

    Parameters
    ----------
    fold_data : str
        Path of the folder containing FOTO results.

    Attributes
    ----------
    zpadf : int,
        Zero padding factor
    n_f : int,
        Number of frequencies (or wavenumbers) taken into account
    mwf : int,
        Moving windows factor.
    w_size_foto : int,
        Window size, in pixels, that is actually taken into account for Fourier
        transform
    w_size : int,
        Window size, in pixel, that was use for windowing input image.
    fold_data : str,
        Path of the folder containing FOTO results.
    """

    # {param: {'pos': position in the list of param, 'pos2': number of
    #                                                       character to skip}}
    POS_PARAM_TO_EXTRACT = {'zpadf': {'pos': -1, 'pos2': 6},
                            'n_f': {'pos': -2, 'pos2': 0},
                            'mwf': {'pos': -3, 'pos2': 0},
                            'w_size_foto': {'pos': -4, 'pos2': 0},
                            'w_size': {'pos': -5, 'pos2': 1},
                            }

    def __init__(self, fold_data):

        self.fold_data = fold_data
        self.load_param()

    def load_param(self,):
        """
        Load input parameters of FOTO and store them in attributes
        """
        self.extract_params_from_foldname(self.fold_data)

    def _extract_param(self, fold_name, param):
        """
        Extract a param from the foldname of output data

        Returns
        -------
        param_value : int
            The parameter value
        """
        param_value = fold_name[self.POS_PARAM_TO_EXTRACT[param]['pos']]
        param_value = param_value[self.POS_PARAM_TO_EXTRACT[param]['pos2']:]
        return int(param_value)

    def extract_params_from_foldname(self, path_fold_data):
        """
        From path of the output data folder extract input parameters of FOTO
        """
        # extract fold name from folder path
        _, name = os.path.split(path_fold_data)
        name, _ = os.path.splitext(name)
        name = name.split('_')

        # extract each param from the fold name
        for param in self.POS_PARAM_TO_EXTRACT.keys():
            setattr(self, param, self._extract_param(name, param))

    def __repr__(self):
        self.report()

    def report(self):
        """
        Print all the parameters of the FOTO analysis
        """
        text = 'Input parameters for FOTO processing :\n'
        metadata = str(self.__dict__).replace(',', '\n')
        metadata = metadata.replace('{', '')
        metadata = metadata.replace('}', '\n')
        text += metadata
        text += 'Number of frequencies can be different from the output ' +\
                'number of frequencies'

        return text


class Fourier(Param_loader):
    """
    Contains results of fourier transform.

    Parameters
    ----------
    fold_data : str
        Path of the folder containing FOTO results.

    Attributes
    ----------
    spectreswima : nd_array
        Contain r_spectra of each window and related information.
        Organization is :

           - Columns 1: windows Id
           - Colums 2-3:  Analysis window position within the image : column,
             line
           - Column 4: Variance within the analysis window
           - Column 5-6 (clat, clong): latitude, longitude (in meters)
           - Columun 7: spatial resolution (in meters)
           - Column 8 to end: radial spectrum for each wavenumber
    r_spectra : nd_array
        r_spectra of each windows.
    nb_f : int
        Number of wavenumber or frequencies.
    """
    _LIST_PARAM = ['spectreswima']

    def __init__(self, fold_data, n_process=1):
        """
        """
        Param_loader.__init__(self, fold_data, self._LIST_PARAM,
                              n_process=n_process)

        self.r_spectra = self.spectreswima[:, 7:]

    @property
    def nb_f(self):
        """ Number of frequencies taken into account"""
        return self.r_spectra.shape[1]


class Fmap(object):
    """
    RGB coding Fmap from PCA scores or mean radiance of windows.

    Parameters
    ----------
    foto : `Foto` instance
        `Foto` instance from which fmap will be contructed

    Attributes
    ----------
    foto : `Foto` instance
        `Foto` instance from which fmap will be contructed
    array : 3d array [heigh, width, 3],
        RGB image.
    nodata : int,
        Nodata value of fmap image
    score : str,
        If raw score of standardized of PCA are to be used.
    srs : osr.SpatialReference object
        The image's projection. The same than foto.in_img.srs.
    transform : (tuple of floats)
        The image's geo-transformation. The same than foto.in_img.transform
    band_info :
        Dictionnary containing what information is put into
        band 1, 2 and 3.
    """

    BAND_INFO = {'polar': {0: 'Theta',
                           1: 'R',
                           -1: 'NDVI'},
                 'XY': {-1: 'NDVI',
                        0: 'PC1',
                        1: 'PC2',
                        2: 'PC3'}}

    def __init__(self, foto):

        self.foto = foto
        self.array = None
        self.nodata = None
        self.score = None
        self.srs = self.foto.in_img.srs
        self.transform = self.foto.in_img.transform
        self.band_info = {'b1': None,
                          'b2': None,
                          'b3': None}

    def _construct_fband(self, num_axis, score=None, nodata=None,
                         ang_offset=None):
        """
        Construct an image band that contains the coordinates of a given PCA
        axis

        Parameters
        ----------
        num_axis : int,
            Number of the PCA axis to use. Indexation starts at 0. If -1, it's
            the mean intensity of the windows that is used.
        nodata : float,
            Empty parts of the image will be filled with nodata value. Defauts
            to -999
        score : {`raw`, `std`, `polcoord`, `None`} (optional)
            If coordinates are to be computed from raw scores or
            standardized scores. Indicate polcoord polar coordinate are to be
            used instead. Default to Foto.score_mode.

        Returns
        -------
        fband : 2d array, [heigh, width]
            Array made of subplots. Each subplot contains the coordinates of
            the num_axis th axis.
        """

        # initialize the array parameters
        out_dt = RasterDataType(lstr_dtype='float32')
        nodata = nodata or self.nodata or -999
        self.nodata = nodata
        fband = sp.ones((self.foto.n_row, self.foto.n_col),
                        dtype=out_dt.numpy_dtype) * nodata

        # get data, either pca scores or mear intensity
        score = score or self.foto.score_mode
        if num_axis == -1:
            self.foto.in_img.veg_lum._print_lum_msg()
            data = self.foto.mean_lum_by_window
            if self.foto.in_img.veg_lum.veg_color == 'dark':
                msg = ('Sign of mean grey level is changed since ' +
                       'vegeation appears as dark in original input image')
                print msg
                data = [- mean_lum for mean_lum in data]
        elif score == 'raw':
            data = self.foto.pca.coorditot[:, num_axis]
        elif score == 'std':
            data = self.foto.pca.std_coorditot[:, num_axis]
        elif score == 'polcoord':
            if num_axis == 0:
                data = self.foto.pol_coord(score='std',
                                           theta_offset=ang_offset)['theta']

            elif num_axis == 1:
                data = self.foto.pol_coord(score='std')['r']

        # fill band
        coord = self.foto.coord
        for i, [r, c] in enumerate(coord):
            id_row = r // self.foto.input_param.w_size
            id_col = c // self.foto.input_param.w_size
            fband[id_row, id_col] = data[i]

        return fband

    def construct_array(self, axes=[1, 2, 3], nodata=None, score=None,
                        ang_offset=None):
        """
        Construct an RGB image that plot the coordinates of the first
        axis of the PCA. In the case where the FOTO method was applied on a
        resampled image, coord of the tiles in the original image must be
        provided in order to rebuild the original image.

        Parameters
        ----------
        axes : int (optional)
            Number of the nth PCA axis to use. Defaults to [1,2,3], if -1 is
            indicated, it's the mean of spectral values that will be used.
            Index starts at 1.
        nodata : float, (optional)
            Empty parts of the image will be filled with nodata value if
            indicated.
        score : {`raw`, `std`, `polcoord`, `None`} (optional)
            If coordinates are to be computed from raw scores or
            standardized scores. Indicate polcoord polar coordinate are to be
            used instead. Default to Foto.score_mode.
        """

        out_dt = RasterDataType(lstr_dtype='float32')
        score = score or self.foto.score_mode
        type_coord = 'polar' if score == 'polcoord' else 'XY'
        self.score = score if score != 'polcoord' else 'std'
        fmap = sp.ones((self.foto.n_row, self.foto.n_col, len(axes)),
                       dtype=out_dt.numpy_dtype)
        axes = [axis - 1 if axis != -1 else -1 for axis in axes]
        ang_offset = ang_offset
        for i, b in enumerate(axes):
            fmap[:, :, i] = self._construct_fband(b,
                                                  nodata=nodata,
                                                  score=score,
                                                  ang_offset=ang_offset)

        self.array = fmap

        band_info = self.BAND_INFO[type_coord]
        self.band_info = {'b1': band_info[axes[0]],
                          'b2': band_info[axes[1]],
                          'b3': band_info[axes[2]]}


    def write_image(self, out_filename=None, resolution=None,
                    nodata=None, x_offset=None, y_offset=None):
        """
        Write fmap image file.

        Parameters
        ----------
        out_filename : str (optional)
            Path of fmap. If not indicated, will be plot at
            Foto.output_fold by default.
        resolution : int, (optional)
            Spatial resolution of fmap in pixels. Defauts to
            Foto.input_param.w_size (that may be different than Foto.w_size)
        nodata : float, (optional)
            Nodata values to set in metadata. Warning do not change actual
            value of nodata that may be present in Foto.fmap_array.
            Default to -999
        """

        nodata = nodata or self.nodata or -999
        suffix = 'fmap_axes_{b1}{b2}{b3}_{score}.tif'.format(score=self.score,
                                                             **self.band_info)
        out_filename = out_filename or join(self.foto.output_fold, suffix)

        # set resolution form ouput fmap into transform medata of input image
        resolution = resolution or self.foto.input_param.w_size
        transform = list(self.transform)
        transform[1], transform[5] = [resolution * self.foto.p_size,
                                      -resolution * self.foto.p_size]
        if x_offset:
            transform[0] += x_offset
        if y_offset:
            transform[3] += y_offset
        self.nodata = nodata
        band_info = [self.band_info['b1'], self.band_info['b2'],
                     self.band_info['b3']]
        write_file(out_filename, array=self.array, srs=self.srs,
                   transform=transform, nodata_value=nodata,
                   band_info=band_info)
        rst = Raster(out_filename)
        self.img = rst
        rst.write_metadata({'PCA_score': self.score})
        rst = None

class Foto(object):
    """Represent FOTO parameters, results of Fourier transform and the
    following PCA

    This class allows for loading result from previous analysis, and perform
    operation to better interpret results.

    Parameters
    ----------
    fold_data : str
        Path of the folder containing FOTO results
    input_img : str (optional)
        Path of the input image.
    output_fold : str (optional)
        Path general output folder where saving results

    Attributes
    ----------
    pca :
        `PCA` instance. Contains results of PCA and method to handle it.
    input_param :
        `ParamFoto` instance. Contains all input param of FOTO analysis
    in_img :
        `InputRaster` instance, contains metadata of input image and methods
        to extracts part of image (or the wole image) as array.
    fourier :
        `Fourier` instance. Contains results of Fourier transfrom and methods
        to handle it.
    score_mode : str ``<'raw', 'std'>``
        Determine if raw scores or standardized scores of axes PCA is to be
        consider for all operations that will append in the current Foto
        instance. Default to `'raw'`.
    output_fold : str
        Path of general output folder where saving results. Default to None.
    coord : array-like (n,2)
        Contain list of pixels' row and column of top lef pixel of each window.
    w_size
    n_col
    n_row
    p_size
    nb_f
    f_km
    grid_id_dict


    Limitations
    -----------
    Currently input paremeters of FOTO analysis are extracted from the name
    of the output folder or FOTO analysis. See also ParamFoto class for
    more details.
    """

    _SCORE_MODE = ('raw', 'std')

    def __init__(self, fold_data, input_img=None, output_fold=None,
                 n_process=1):

        self.pca = Pca(fold_data, n_process=n_process)
        self.input_param = ParamFoto(fold_data)
        if input_img:
            self.in_img = input_img
        else:
            self._in_img = None
        self.fourier = Fourier(fold_data, n_process=n_process)
        self.n_win = self.pca.coorditot.shape[0]
        self.score_mode = 'std'
        self._output_fold = output_fold
        self.n_process = n_process

    def __str__(self,):

        return self.input_param.report()

    def init_in_img_parameters(self,):
        """
        TODO
        """

        self._n_col = self.in_img.width // self.input_param.w_size
        self._n_row = self.in_img.height // self.input_param.w_size
        self._p_size = self.in_img.transform[1]

    @property
    def output_fold(self,):
        """
        """
        if self._output_fold is None:
            raise AttributeError('Foto.output_fold has not been set yet')
        return self._output_fold

    @output_fold.setter
    def output_fold(self, output_fold):
        """
        """
        self._output_fold = output_fold

    @property
    def score_mode(self):
        """
        Define if treatments and display will be done from raw scores or
        standardized scores. Can be set to `raw` or `std`.
        """
        return self._score_mode

    @score_mode.setter
    def score_mode(self, score_mode):

        if score_mode not in self._SCORE_MODE:
            msg = "'score_mode' only can be set to one of these option : {}"
            raise ValueError(msg.format(self._SCORE_MODE))
        self._score_mode = score_mode

    @property
    def in_img(self):
        """
        `InputRaster` instance, contains metadata of input image and methods
        to extracts part of image (or the wole image) as array
        """
        return self._in_img

    @in_img.setter
    def in_img(self, input_img):
        """
        in_img setter

        Parameters
        ----------
        input_img : string,
            Path of input_img used as input of FOTO
        """
        self._in_img = InputRaster(input_img)
        self.init_in_img_parameters()

    @property
    def coord(self):
        """
        TODO : documentation, allow for defaults coordinnates (in case all tiles
        are to be taken into account)
        """
        try:
            return self._coord
        except AttributeError:
            raise AttributeError('ERROR : tiles\' coordinates Foto.coord not' +
                                 'loaded or indicated')

    @coord.setter
    def coord(self, coord):

        if isinstance(coord, sp.ndarray) or isinstance(coord, list):
            self._coord = coord
        else:
            msg = 'Error : coord type is expected to be numpy.ndarray, or' +\
                    'list, not {}'.format(type(coord))
            print(msg)

    @property
    def w_size(self):
        """The Foto windows size"""
        try:
            return self._w_size
        except AttributeError:
            return self.input_param.w_size_foto

    @w_size.setter
    def w_size(self, w_size):
        """Set the windows size in pixel
        """
        self._w_size = w_size

    @property
    def n_col(self):
        """The Foto number of columns"""

        return self._n_col

    @n_col.setter
    def n_col(self, n_col):
        """Set Foto number of columns"""

        self._n_col = n_col

    @property
    def n_row(self):
        """ The Foto number of rows"""

        return self._n_row

    @n_row.setter
    def n_row(self, n_row):
        """Set Foto number of row """

        self._n_row = n_row

    @property
    def p_size(self):
        """"The pixel size of the input image"""

        return self._p_size

    @p_size.setter
    def p_size(self, p_size):
        """
        SetThe pixel size of the input image
        """
        self._p_size = p_size

    @property
    def grid_id_dict(self):

        try:
            return self._grid_id_dict
        except AttributeError:

            top_x = self.coord[:, 1]
            top_y = self.coord[:, 0]
            id_col = top_x // self.input_param.w_size
            id_row = top_y // self.input_param.w_size
            id_grid = id_row * self.n_col + id_col
            self._grid_id_dict = {'id_col': list(id_col),
                                  'id_row': list(id_row),
                                  'id_grid': list(id_grid),
                                  'top_x': list(top_y),
                                  'top_y': list(top_x)}

            return self._grid_id_dict

    @property
    def cycles_km(self):
        """Returns the frequencies in cycles.km-1
        """
        return [int(round((f * 1000) /
                          (self.input_param.w_size_foto * self.p_size)))
                for f in range(1, self.fourier.nb_f + 1)]

    def copy(self):
        """
        Return a deep copy of itself
        """
        fold_data = self.input_param.fold_data
        in_img = self.in_img.filename if self.in_img else None
        output_fold = self._output_fold if self._output_fold else None

        f_copy = Foto(fold_data, input_img=in_img, output_fold=output_fold,
                      n_process=self.n_process)
        f_copy.score_mode = self.score_mode
        try:
            f_copy._mean_lum_by_window = self._mean_lum_by_window
        except AttributeError:
            pass
        try:
            f_copy.coord = self._coord
        except AttributeError:
            pass
        if self.in_img.filename:
            f_copy.in_img.veg_lum = self.in_img.veg_lum
        return f_copy

    def _check_orientation(self):
        """
        Check the orientation of the principal components, regarding to the
        sign of the contribution of the first frequence.

        Returns
        -------
        check_axis : list of bool,
            Boolean list of the axis. True if the contribution of the first
            variable is positif, else false

        """

        check_axis = [True if self.pca.cosv[0, ax] >= 0 else False
                      for ax in range(self.fourier.nb_f)]

        return check_axis

    def change_orientation(self, axes=[1, 2, 3]):
        """
        TODO : doc and score_mode
        """
        check_axis = self._check_orientation()
        list_change = []
        for ax in axes:
            ax = ax - 1  # indexations starts at 0
            if not check_axis[ax]:
                self.pca.cosv[:, ax] = - self.pca.cosv[:, ax]
                self.pca.coorditot[:, ax] = - self.pca.coorditot[:, ax]
                list_change.append(str(ax + 1) + ', ')
            else:
                self.eigvec[:, ax] = - self.eigvec[:, ax]

        if list_change:
            print 'Change(s) on axe(s) : {}.'.format(list_change)
        else:
            print 'No change done.'

        return self

    def _extract_window_from_pixel_coord(self, coord=None):
        """
        Parameters
        ----------
        coord : list of tuple, (id_row, id_col), optional
            Contains the list of the coord of the first pixel (top left) of
            tile of the resamp img to extract. Extract all windows if not
            indicated.

        Returns
        -------
        tab : list of 2d np.array
            List of windows.
        """
        # try to load list of windows array and compute it if necessary
        if coord is None :
            save_array = False
            coord = self.coord
        else:
            save_array = True

        my_id = generate_id_from_coord(coord)
        _, filename = os.path.split(self.in_img.filename)
        try:
            file_tab = join(self.output_fold,
                            'list_windows_as_array_{}_{}.npy'.format(filename,
                                                                     my_id))
            tab = list(sp.load(file_tab))
        except IOError:
            try:
                self.in_img
            except AttributeError:
                error_message = ('Error : you need to specify the reference' +
                                 ' image Foto.in_img')
                raise AttributeError(error_message)

            coord = coord if coord is not None else self.coord
            w_size = self.input_param.w_size_foto
            tab = [self.in_img.array_from_bands(block_win=(x, y, w_size,
                                                           w_size))
                   for (y, x) in coord]

            # save and create ouput fold if necessary
            if save_array:
                array_name = 'list_windows_as_array_{}_{}.npy'.format(filename,
                                                                      my_id)
                array_filename = join(self.output_fold, array_name)
                try:
                    sp.save(array_filename, tab)
                except IOError:
                    os.makedirs(self.output_fold)
                    sp.save(array_filename, tab)
        return tab

    def window_from_win_grid_id(self, list_win_grid_id):
        """
        """
        df = pd.DataFrame.from_dict(self.grid_id_dict)
        coord = df.loc[df['id_grid'].isin(list_win_grid_id),
                       ['top_x', 'top_y']].values

        list_windows = self._extract_window_from_pixel_coord(coord=coord)
        list_win_grid_id = df.loc[df['id_grid'].isin(list_win_grid_id),
                       ['id_grid']].values
        return list_windows, list_win_grid_id

    @property
    def mean_lum_by_window(self):
        """
        TODO
        """
        try:
            return self._mean_lum_by_window
        except AttributeError:
            try:
                coord = self.coord
                my_id = generate_id_from_coord(coord)
                _, filename = os.path.split(self.in_img.filename)
                mean_file = join(self.output_fold,
                                 'list_mean_lum_{}_{}.npy'.format(filename,
                                                                  my_id))
                self._mean_lum_by_window = list(sp.load(mean_file))
            except IOError:
                tab = self._extract_window_from_pixel_coord()
                self._mean_lum_by_window = [sp.mean(win) for win in tab]
                sp.save(mean_file, self._mean_lum_by_window)
        return self._mean_lum_by_window

    def pol_coord(self, score=None, theta_offset=None):
        """
        Get polar coordinates of windows in the plane define by the two
        first principale component.

        Parameters
        ----------
        score = string {`raw`,`std`} (optional)
            If coordinates are to be computed from raw scores or
            standardized scores. Default to Foto.score_mode.
        theta_offset : float (optional)
            Offset to apply, in degrees

        Returns
        -------
        pol : dictionnary of array.
            Polar coordinates : ::

                {'theta': array of theta (in radians),
                 'r' : array of distance from center
                }

        """

        score = score or 'std'
        if score == 'raw':
            coord = self.pca.coorditot
        elif score == 'std':
            coord = self.pca.std_coorditot

        x = coord[:, 0]
        y = coord[:, 1]
        polar = pol_coord_from_xy(x, y)
        if theta_offset is not None:
            offset = lambda x: lib.ang_offset(x, theta_offset)
            polar['theta'] = sp.apply_along_axis(offset, 1,
                polar['theta'].reshape(-1, 1))
        return polar


    def frequencies_by_ang_class(self, **kwargs):
        """
        Parameters
        ----------
        classes : list of angles ``[[ang_min,ang_max], [,],...]`` or int, optional
            List of the radial classes, expressed in positive radian. A single
            value can also be indicated. In the case where an integer is
            indicated, 'classes' equal angular classes will be created. For
            instance if ``'classes' = 8``, 8 classes of pi/4 rad are created.
            By default, 8 classes are created, the first is [0,pi/4].
        """
        # Define classes/home/marc
        cla = kwargs.get('classes')
        if isinstance(cla, list):
            classes = cla
        elif isinstance(cla, int) or cla is None:
            classes = self._create_ang_classes(nb_class=cla)

        pol = self.pol_coord_of_correlation()
        tab_class = []
        freq = sp.asarray(self.cycles_km)
        for i, (lim_inf, lim_sup) in enumerate(classes):
            condition = (pol['theta'] > lim_inf) * (pol['theta'] < lim_sup)
            coord_cla = sp.where(condition)
            tab_class.append(freq[coord_cla])

        return tab_class

    def pol_coord_of_correlation(self,):
        """
        Get polar coordinates of the correlation between frequencies and
        PC 1 and 2.

        Returns
        -------
        pol : dictionnary of array.
            Polar coordinates : ::

                {'theta': array of theta (in radians),
                 'r' : array of distance from center
                }
        """

        x = self.pca.cosv[:,0]
        y = self.pca.cosv[:,1]

        return pol_coord_from_xy(x, y)

    def _get_lum_class(self, lum_classes_mode, lum_classes_th):
        """
        """
        mean_lum = sp.asarray(self.mean_lum_by_window)
        lum_classes = sp.empty((self.n_win))
        for i, (lim_inf, lim_sup) in enumerate(lum_classes_th):
            if lum_classes_mode == 'percentile':
                lim_inf = sp.percentile(mean_lum, lim_inf)
                lim_sup = sp.percentile(mean_lum, lim_sup)
            elif lum_classes_mode == 'absolute':
                pass
            else:
                msg = ("'lum_classes_mode' only can be set to one of these" +
                       "option : 'absolute' or 'percentile'")
                raise ValueError(msg)
            condition = ((mean_lum >= lim_inf) *
                         (mean_lum < lim_sup))
            coord_cla = sp.where(condition)
            lum_classes[coord_cla] = i + 1

        return lum_classes

    def _get_angular_class(self, nb_class, score=None):
        """
        Return angular class for each window

        Parameters
        ----------
        nb_class : int,
            Number of angular classes to create.
        score = string {`raw`,`std`} (optional)
            If coordinates are to be computed from raw scores or
            standardized scores. Default to Foto.score_mode.
        Returns
        -------
        tab_class : 1d array.
            Array containing class number of each windows from 0 to `nb_class`
            , starting from the 0rad classe.
        """

        list_class = self._create_ang_classes(nb_class=nb_class)
        score = score or self.score_mode
        pol = self.pol_coord(score=score)
        tab_class = sp.empty((self.n_win))
        for i, (lim_inf, lim_sup) in enumerate(list_class):
            condition = (pol['theta'] > lim_inf) * (pol['theta'] < lim_sup)
            coord_cla = sp.where(condition)
            tab_class[coord_cla] = i + 1

        return tab_class

    def _create_ang_classes(self, nb_class=None):
        """
        Create a list of angular classes with an inferior and superior
        boundaries expressed in radian.

        Parameters
        ----------
        nb_class : int,
            Number of angular classes to create. Default to 8.

        Returns
        -------
        classes : list of angles ``[[ang_min,ang_max], [,],...]``
            List of angulars classes boudaries
        """
        nb_class = nb_class or 8
        pi = sp.pi
        rg = 2 * pi / nb_class
        list_class = [[i * rg, (i+1) * rg] for i in range(nb_class)]
        return list_class

    def _get_lum_class_label(self, lum_classes_th):
        """
        Return string label of float classes' limits

        Parameters
        ----------
        lum_classes_th : list of tupe of flaat, (optional)
            List of luminance classe expressend in percentile or absolute
            value depending on the `lum_classes_mode` chosen. E.g.
            ``[[0,33], [33,66],...]`` for percentiles.

        Returns
        -------
        lum_class_label: list of string
            The labels of the luminances classe corresponding to the limits
            of each class.
        """
        lum_class_label = []
        for lim_inf, lim_sup in lum_classes_th:
            label = '[{:.2f} - {:.2f}]'.format(lim_inf, lim_sup)
            lum_class_label.append(label)
        return lum_class_label

    def select_win_id_by_mean_lum(self, lum_classes_mode, lum_classes_th):
        """
        Return windows ID stored of windows sorted according to their mean
        luminance values.

        Parameters
        ----------
        lum_classes_mode : {`False`, `percentile`, `absolute`}, (optional)
            If windows must be selected according to their mean luminance.
            Mean luminance classes can be define by percentile or by
            abolute value. If False, windows will be selected by angular
            sector only, whatever their mean luminance value. False by
            default.
        lum_classes_th : list of tupe of flaat, (optional)
            List of luminance classe expressend in percentile or absolute
            value depending on the `lum_classes_mode` chosen. E.g.
            ``[[0,33], [33,66],...]`` for percentiles.

        Returns
        -------
        win_id : list of 1d array,
            ID of the windows per lum class. Each list element contains the
            Id of a given lum classe, ID reffering to the indexation of the
            Foto.coorditot table.
        lum_classes : list of tuple of float
            list of the limits (inf and sup) of each luminances classe
        """
        mean_lum = sp.asarray(self.mean_lum_by_window)
        win_id = []
        lum_classes = []
        for lim_inf, lim_sup in lum_classes_th:
            if lum_classes_mode == 'percentile':
                lim_inf = sp.percentile(mean_lum, lim_inf)
                lim_sup = sp.percentile(mean_lum, lim_sup)
            elif lum_classes_mode == 'absolute':
                pass
            else:
                msg = ("'lum_classes_mode' only can be set to one of these" +
                       "option : 'absolute' or 'percentile'")
                raise ValueError(msg)
            condition = ((mean_lum >= lim_inf) *
                         (mean_lum < lim_sup))
            win_id.append(sp.where(condition))
            lum_classes.append((lim_inf, lim_sup))

        return win_id, lum_classes



    def select_ang_img(self, **kwargs):
        """
        Select the windows that have the extremum coordinates in a given
        angular sector, in given luminance class

        Parameters
        ----------
        classes : list of angles ``[[ang_min,ang_max], [,],...]`` or int, optional
            List of the radial classes, expressed in positive radian. A single
            value can also be indicated. In the case where an integer is
            indicated, 'classes' equal angular classes will be created. For
            instance if ``'classes' = 8``, 8 classes of pi/4 rad are created.
            By default, 8 classes are created, the first is [0,pi/4].
        lum_classes_mode : {`False`, `percentile`, `absolute`}, (optional)
            If windows must be selected according to their mean luminance.
            Mean luminance classes can be define by percentile or by
            abolute value. If False, windows will be selected by angular
            sector only, whatever their mean luminance value. False by
            default.
        lum_classes_th : list of tupe of flaat, (optional)
            List of luminance classe expressend in percentile or absolute
            value depending on the `lum_classes_mode` chosen. E.g.
            ``[[0,33], [33,66],...]`` for percentiles.
        n_w : int, (optional)
            Number of windows to select. Should not be indicated if thresh is
            indicated.
        thresh: int, (optional)
            Distance thresh from the center of the PCA plan - expressed in
            percentage of maximum distance- beyond which windows are selected.
            Should not be indicated if n_w is indicated.
        output_classes : bool, (optional)
            If True, return the classes' limits. False by default
        score : string {`raw`,`std`} (optional)
            If coordinates are to be computed from raw scores or
            standardized scores. Default to Foto.score_mode.
        r_max : int (optional)
            Windows with a distance to the center greater than r_max
            won't be consider for the selection. R_max is expressed in
            percentile and compute for each angular classif. By default all
            r_max is inf.
        Returns
        -------
        sel_by_lum : dict of list of 1d array, {lum_class : list of array}
            ID of the windows selected. Each list element contains the Id of a
            given angular classe, ID reffering to the indexation of the
            Foto.coorditot table. List of ID are stored in a dict whose
            keys are the lum_labels.
        classes : classes : list of angles ``[[ang_min,ang_max], [,],...]`` ,
        optional
            List of the radial classes, expressed in positive radian.
        grid_id_by_lum : {lum_class : list of int},
            grid id of the selected arrays corresponding to the windows whose
            distance to the center is maximal. List of grid ID are stored in a
            dict whose keys are the lum_labels.
        lum_labels: list of string
            The labels of the luminances classe corresponding to the limits
            of each class.
        """

        # Define classes
        cla = kwargs.get('classes')
        if isinstance(cla, list):
            classes = cla
        elif isinstance(cla, int) or cla is None:
            classes = self._create_ang_classes(nb_class=cla)

        # get id of windows for each classes
        sel_img = []
        score = kwargs.get('score') or self.score_mode
        pol = self.pol_coord(score=score)
        n_w = kwargs.get('n_w')
        thresh_percentage = kwargs.get('thresh')

        if kwargs.get('lum_classes_mode'):
            lum_classes_mode = kwargs.get('lum_classes_mode')
            lum_classes_th = kwargs.get('lum_classes_th')
            # get window for each lum class
            list_coord_by_lum, lum_classe = self.select_win_id_by_mean_lum(
                lum_classes_mode, lum_classes_th)
            print list_coord_by_lum, lum_classe
            lum_class_labels = self._get_lum_class_label(lum_classe)

            # get polar coordinates for each lum class
            list_pol_by_lum = []
            for lum_i in list_coord_by_lum:
                pol_by_lum = {'theta': pol['theta'][lum_i],
                              'r': pol['r'][lum_i]}
                list_pol_by_lum.append(pol_by_lum)
        else:
            list_pol_by_lum = [pol]
            lum_class_labels = ['All values']
            list_coord_by_lum = [(sp.arange(self.n_win),)]
        sel_by_lum = {}
        # for each lum class
        lum_iterator = zip(list_coord_by_lum, list_pol_by_lum,
                           lum_class_labels)
        for coord_by_lum, pol, lum_class_label in lum_iterator:
            sel_img = []
            # for each angular class limits
            for i, (lim_inf, lim_sup) in enumerate(classes):

                # set 0 to radius that are not in the current angular class
                condition = (pol['theta'] > lim_inf) * (pol['theta'] < lim_sup)
                r_in_current_cla = sp.where(condition, pol['r'], 0)
                if kwargs.get('r_max'):
                    copy = r_in_current_cla.copy()
                    copy[copy==0] = sp.nan
                    r_max = sp.nanpercentile(copy,
                                          kwargs.get('r_max'))
                    max_before = r_in_current_cla.max()
                    condi = r_in_current_cla > r_max
                    n_delete = r_in_current_cla[condi].shape
                    r_in_current_cla[condi] = 0
                    print ('rmax set to {} instead of ' +
                           '{} for class {}, lum {}').format(r_max,
                                max_before, i, lum_class_label)
                    print ('{} windows were removed').format(n_delete)
                    #condition = (r_in_current_cla > r_max)
                    #r_in_current_cla = sp.where(condition)
                if n_w:
                    # selection of th en_w th greatest radius in the current
                    # class
                    #coord_ok = sp.argpartition(r_in_current_cla, - n_w)[- n_w:]
                    coord_ok = r_in_current_cla.argsort()[::-1][:n_w]
                    coord_ok = coord_by_lum[0][coord_ok]
                elif thresh_percentage:
                    # select radius greater than tresh % of total distance from
                    # from the center
                    dist_max = r_in_current_cla.max()
                    thresh = dist_max * thresh_percentage / 100
                    coord_ok = sp.where(r_in_current_cla >= thresh)[0]
                    coord_ok = coord_by_lum[0][coord_ok]
                # add the current angular class selection
                sel_img.append(coord_ok)

            # add the current lum class selection
            sel_by_lum[lum_class_label] = sel_img

        # get grid id of selected windows
        grid_id_by_lum = {}
        # for each lum class
        for lum_class_label, sel_img in sel_by_lum.iteritems():
            list_grid_id = []

            # for the list of window id of each anglular class get the grid
            # id list
            for win_ids in sel_img:
                grid_ids = [self.grid_id_dict['id_grid'][win_id]
                            for win_id in win_ids]
                list_grid_id.append(grid_ids)
            grid_id_by_lum[lum_class_label] = list_grid_id

        if kwargs.get('output_classes'):
            return sel_by_lum, grid_id_by_lum, classes, lum_class_labels
        else:
            return sel_by_lum, grid_id_by_lum

    def get_extrem_angular_window(self, **kwargs):
        """
        Get the windows whose distance to the center
        of the PCA plan is maximal, by angular sector and by mean luminance
        range.

        Parameters
        ----------
        classes : list of angles ``[[ang_min,ang_max], [,],...]`` or int,
        optional
            List of the radial classes, expressed in positive radian. A single
            value can also be indicated. In the case where an integer is
            indicated, 'classes' equal angular classes will be created. For
            instance if ``'classes' = 8``, 8 classes of pi/4 rad are created.
            By default, 8 classes are created, the first is [0,pi/4].
        lum_classes_mode : {`False`, `percentile`, `absolute`}, (optional)
            If windows must be selected according to their mean luminance.
            Mean luminance classes can be define by percentile or by
            abolute value. If False, windows will be selected by angular
            sector only, whatever their mean luminance value. False by
            default.
        lum_classes_th : list of tupe of flaat, (optional)
            List of luminance classe expressend in percentile or absolute
            value depending on the `lum_classes_mode` chosen. E.g.
            ``[[0,33], [33,66],...]`` for percentiles.
        n_w : int, (optional)
            Number of windows to select. Should not be indicated if thresh is
            indicated.
        thresh: int, (optional)
            Distance thresh from the center of the PCA plan - expressed in
            percentage of maximum distance- beyond which windows are selected.
            Should not be indicated if n_w is indicated.
        score : string {`raw`,`std`} (optional)
            If coordinates are to be computed from raw scores or
            standardized scores. Default to Foto.score_mode.
        r_max : int, betwee 0 and 100 (optional)
            Windows with a distance to the center greater than r_max
            won't be consider for the selection. R_max is expressed in
            percentile and compute for each angular classif. By default all
            r_max is inf.

        Returns
        -------
        dictionnary that contains for the following keys :
            windows : dict of list of 1d array;
                {lum_class : list of array}, the array
                corresponding to the windows whose distance to the center is
                maximal.
            classes: list of tuple of float,
                List of the limits (inf and sup) in degrees of the angulars
                classes
            windows_id:
                {lum_class : list of int}, id of the arrays
                corresponding to the windows whose distance to the center is
                maximal.
            lum_labels: list of string
                The labels of the luminances classe corresponding to the limits
                of each class.
        """
        classes = kwargs.get('classes')
        thresh = kwargs.get('thresh')
        lum_classes_mode = kwargs.get('lum_classes_mode')
        lum_classes_th = kwargs.get('lum_classes_th')
        r_max = kwargs.get('r_max')

        # get extrem windows id
        score = kwargs.get('score') or self.score_mode
        (extrem_win_id,
         extrem_grid_id,
         rad_cla,
         lum_labels) = self.select_ang_img(classes=classes,
                                           n_w=kwargs.get('n_w'),
                                           thresh=thresh, output_classes=True,
                                           score=score,
                                           lum_classes_mode=lum_classes_mode,
                                           lum_classes_th=lum_classes_th,
                                           r_max=r_max)

        # convert class limit in degree
        deg_cla = [[int(math.degrees(lim_inf)), int(math.degrees(lim_sup))]
                   for [lim_inf, lim_sup] in rad_cla]

        # extract extrem windows
        sub_plots_by_lum = {}
        coord = self.coord
        for lum_label in lum_labels:
            print lum_label
            print extrem_win_id[lum_label]
            list_sub_plots = [self._extract_window_from_pixel_coord(
                                coord[coord_i])
                              for coord_i in extrem_win_id[lum_label]]
            sub_plots_by_lum[lum_label] = list_sub_plots

        # convert window id in grid id
        dict_subplot = {'windows': sub_plots_by_lum,
                        'classes': deg_cla,
                        'windows_id': extrem_win_id,
                        'lum_labels': lum_labels
                        }
        return dict_subplot


    def get_df_data(self, n_axis=2, theta_offset=0, mean_lum=False, score='std'):
        """
        Parameters
        ----------
        n_axis : int
            number of axis to extract.Default to 2.
        theta_offset : float (optional)
            Offset to apply, in degrees
        score : <'std', 'raw'>
            If raw or standardized score must be picked
        """

        if score=='std':
            coord = self.pca.std_coorditot[:, :n_axis]
            pol_coord = self.pol_coord(score='std',
                                       theta_offset=theta_offset)
        elif score == 'raw':
            coord = self.pca.coorditot[:, :n_axis]
            pol_coord = self.pol_coord(score='raw',
                                       theta_offset=theta_offset)

        theta = pol_coord['theta'].reshape((-1, 1))
        r = pol_coord['r'].reshape((-1, 1))
        grid = sp.asarray(self.grid_id_dict['id_grid'])
        columns = ['Axe {}'.format(i + 1) for i in range(n_axis)]
        columns.extend(['Theta', 'R'])
        if mean_lum:
            mean_lum_data = self.mean_lum_by_window
            mean_lum_data = sp.asarray(mean_lum_data).reshape((-1, 1))
            data = sp.concatenate((coord, theta, r, mean_lum_data, self.coord),
                                  axis=1)
            columns.extend(['Mean lum', 'row', 'col'])
            f_data = pd.DataFrame(data=data, index=grid, columns=columns)
        else:
            data = sp.concatenate((coord, theta, r), axis=1)
            f_data = pd.DataFrame(data=data, index=grid, columns=columns)

        return f_data


    @property
    def displayer(self,):
        """
        Return a displayer instance of the present Foto instance
        """

        return Foto_displayer(self)

    @property
    def fmap(self,):
        """
        Return a Fmap instance of the present Foto instance
        """

        return Fmap(self)

